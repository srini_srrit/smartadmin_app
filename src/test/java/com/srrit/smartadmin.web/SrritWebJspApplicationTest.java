package com.srrit.smartadmin.web;

import com.srrit.smartadmin.web.app.constants.EmailConstants;
import com.srrit.smartadmin.web.bean.EmailBean;
import com.srrit.smartadmin.web.domain.*;
import com.srrit.smartadmin.web.repository.CustomerRepository;
import com.srrit.smartadmin.web.repository.EmployeeRepository;
import com.srrit.smartadmin.web.repository.UserRepository;
import com.srrit.smartadmin.web.repository.VendorRepository;
import com.srrit.smartadmin.web.service.EmailService;
import com.srrit.smartadmin.web.util.HashGenerator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SmartAdminBootApplication.class)
@WebAppConfiguration
@ActiveProfiles("local")
public class SrritWebJspApplicationTest {

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailService emailService;

    private Vendor vendor;
    private Employee employee;
    private Customer customer;

    @Before
    public void setup(){

        User user = new User();
        user.setStatus(1);
        user.setUsername("username");
        user.setPassword("password");

        Bank bank =new Bank();
        bank.setBankName("bank-name");
        bank.setContactName("contact-name");


        Address address =new Address();
        address.setAddress1("address1");
        address.setCity("city");

        vendor = new Vendor();
        vendor.setContactName("Srini");
        vendor.setUser(user);
        vendor.setAddress(address);

        employee =new Employee();
        employee.setAddress(address);
        employee.setUser(user);
        employee.setBank(bank);

        customer =new Customer();
        customer.setUser(user);
        customer.setBank(bank);
        customer.setAddress(address);
    }

    @Test
    public void testSaveVendor(){

        vendorRepository.save(vendor);
    }

    @Test
    public void testSaveUser(){

        User user = new User();
        user.setStatus(1);
        user.setUsername("kpsvas@gmail.com");
        user.setPassword(HashGenerator.encryptPassword("kpsvas"));
        user.setEmail("kpsvas@gmail.com");
        user.setIsVerified(1);
        user.setRole(Role.ADMIN);

        userRepository.save(user);
    }

    @Test
    public void testSaveCustomer(){

        customerRepository.save(customer);
    }

    @Test
    public void testSaveEmployee(){

        employeeRepository.save(employee);
    }


    @Test
    public void testGetVendors(){

        Iterable<Vendor> vendors =  vendorRepository.findAll();
        System.out.println(vendors);

    }

    @Test
    public void testGetCustomers(){

        Iterable<Customer> customers =  customerRepository.findAll();
        System.out.println(customers);

    }

    @Test
    public void testGetEmployees(){

        Iterable<Employee> employees =  employeeRepository.findAll();
        System.out.println(employees);

    }

	@Ignore
	public void testEmailTemplate() {

		EmailBean email = new EmailBean();
		User user = new User();
		user.setPassword("Srini");
		user.setEmail("kpsvas@gmail.com");
		email.setUser(user);

		email.setSubject(EmailConstants.CONTACTUS_EMAIL_SUBJECT);
		email.setToEmail(EmailConstants.emailTo);
		emailService.sendEmail(email, EmailConstants.EMAILBODY_FEEDBACK);
	}
}
