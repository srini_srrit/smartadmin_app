<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
<div class="content-wrapper" style="min-height: 1200.88px;">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>General Form</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">General Form</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container">
         <div class="row">
            <!-- left column -->
            <div class="col-md-3"></div>
            <div class="col-md-6">
               <div class="form-group row">
                  <label for="input3" class="col-sm-2 col-form-label">Select</label>
                  <div class="col-sm-6">
                     <select id="selectCategory" class="custom-select">
                        <option value="employee">New Employee</option>
                        <option value="vendor">New Vendor</option>
                        <option value="customer">New Customer</option>
                        <option value="bank">New Bank</option>
                     </select>
                  </div>
               </div>
               <div id="showEmp" class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add Employee</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form:form class="form-horizontal" action="${baseUrl}addEmployee" method="post" modelAttribute="employeeDTO">
                     <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="firstname" class="col-sm-4 col-form-label">First Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="firstName" class="form-control" id="firstname" placeholder="First Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="lastname" class="col-sm-4 col-form-label">Last Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="lastName" class="form-control" id="lastname" placeholder="Last Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="Email" class="col-sm-4 col-form-label">Email</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="userDTO.email" class="form-control" id="email" placeholder="Email"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="col-sm-4 col-form-label">Address</label>
                           <div class="col-sm-10">
                              <input id="location-input" class="form-control" placeholder="Address" maxlength="80" autocomplete="off">
                           </div>
                        </div>
                        <form:hidden path="addressDTO.address1" id="address1" />
                        <form:hidden path="addressDTO.city" id="city" />
                        <form:hidden path="addressDTO.state" id="state" />
                        <form:hidden path="addressDTO.country" id="country" />
                        <form:hidden path="addressDTO.zip" id="zipcode" />
                        <div class="form-group row">
                           <label for="email3" class="col-sm-4 col-form-label">Job Title</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="jobTitle" class="form-control" id="jobTitle" placeholder="Job Title"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="mobile" class="col-sm-4 col-form-label">Phone Number</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="phone" class="form-control" id="mobile" placeholder="Phone Number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="mobile" class="col-sm-4 col-form-label">About Me</label>
                           <div class="col-sm-10">
                              <form:textarea path="about" class="form-control" id="aboutMe" placeholder="About Me"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="number" class="col-sm-4 col-form-label">Education Details</label>
                           <div class="col-sm-10">
                              <form:textarea class="form-control" path="educationDetails" id="educationDetails" placeholder="Education Details"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="number" class="col-sm-4 col-form-label">Skill Set</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="skills" class="form-control" id="skillSet" placeholder="Skill Set"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="number" class="col-sm-4 col-form-label">Notes</label>
                           <div class="col-sm-10">
                              <form:textarea class="form-control" path="notes" id="notes" placeholder="Notes"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <div class="offset-sm-2 col-sm-10">
                              <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                     <!-- /.card-body -->
                  </form:form>
               </div>
               <div id="showBank" class="card card-primary" style="display:none;">
                  <div class="card-header">
                     <h3 class="card-title">Add Bank</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form:form class="form-horizontal" action="/addBank" method="post" modelAttribute="bankDTO">
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="firstname" class="col-sm-4 col-form-label">Bank Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="bankName" class="form-control" id="bankname" placeholder="Bank Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="lastname" class="col-sm-4 col-form-label">Contact Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="contactName" class="form-control" id="firstname" placeholder="Contact Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="col-sm-4 col-form-label">Routing Number</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="routingNum" class="form-control" id="routing" placeholder="Routing Number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="col-sm-4 col-form-label">Account Number</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="accountNum" class="form-control" id="account" placeholder="Account Number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="col-sm-4 col-form-label">Address</label>
                           <div class="col-sm-10">
                              <input id="location-input" class="form-control" placeholder="Address" maxlength="80" autocomplete="off">
                           </div>
                        </div>
                        <form:hidden path="addressDTO.address1" id="address1" />
                        <form:hidden path="addressDTO.city" id="city" />
                        <form:hidden path="addressDTO.state" id="state" />
                        <form:hidden path="addressDTO.country" id="country" />
                        <form:hidden path="addressDTO.zip" id="zipcode" />
                        <div class="form-group row">
                           <label for="email3" class="col-sm-4 col-form-label">E-Mail</label>
                           <div class="col-sm-10">
                              <form:input type="email" path="userDTO.email" class="form-control" id="email3" placeholder="E-mail"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="mobile" class="col-sm-4 col-form-label">Contact Number</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="contactNum" class="form-control" id="phone" placeholder="Contact Number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="mobile" class="col-sm-4 col-form-label">Fax</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="fax" class="form-control" id="fax" placeholder="Fax"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="mobile" class="col-sm-4 col-form-label">Website</label>
                           <div class="col-sm-10">
                              <form:input type="text" class="form-control" path="website" id="website" placeholder="Website"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="mobile" class="col-sm-4 col-form-label">About</label>
                           <div class="col-sm-10">
                              <form:textarea class="form-control" path="about" id="about" placeholder="About"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="number" class="col-sm-4 col-form-label">Notes</label>
                           <div class="col-sm-10">
                              <form:textarea class="form-control" path="notes" id="notes" placeholder="Notes"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <div class="offset-sm-1 col-sm-10">
                              <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                     <!-- /.card-body -->
                  </form:form>
               </div>
               <div id="showVendor" style="display:none;" class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add Vendor</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form:form class="form-horizontal" action="/addVendor" method="post" modelAttribute="vendorDTO">
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="companyname" class="col-sm-4 col-form-label">Company Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="companyName" class="form-control" id="companyname" placeholder="Company Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="companyname" class="col-sm-4 col-form-label">Vendor Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="vendorName" class="form-control" id="companyname" placeholder="Vendor Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="email" class="col-sm-4 col-form-label">Email</label>
                           <div class="col-sm-10">
                              <form:input type="email" path="secEmail" class="form-control" id="email" placeholder="EmailId"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="phone" class="col-sm-4 col-form-label">Phone</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="mobile" class="form-control" id="phone" placeholder="Phone number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="fax" class="col-sm-4 col-form-label">Fax</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="fax" class="form-control" id="fax" placeholder="Fax"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="website" class="col-sm-4 col-form-label">Website</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="website" class="form-control" id="website" placeholder="website"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="about" class="col-sm-4 col-form-label">About</label>
                           <div class="col-sm-10">
                              <form:textarea path="about" class="form-control" id="about" placeholder="enter details"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="col-sm-4 col-form-label">Address</label>
                           <div class="col-sm-10">
                              <input id="location-input" class="form-control" placeholder="Address" maxlength="80" autocomplete="off">
                           </div>
                        </div>
                        <form:hidden path="addressDTO.address1" id="address1" />
                        <form:hidden path="addressDTO.city" id="city" />
                        <form:hidden path="addressDTO.state" id="state" />
                        <form:hidden path="addressDTO.country" id="country" />
                        <form:hidden path="addressDTO.zip" id="zipcode" />
                        <div class="form-group row">
                           <label for="associated" class="col-sm-6 col-form-label">Associated Prime Vendors</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="associatedVendor" class="form-control" id="associated" placeholder="enter details"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="notes" class="col-sm-3 col-form-label">Notes</label>
                           <div class="col-sm-10">
                              <form:textarea class="form-control" path="notes" id="notes" placeholder="enter notes"/>
                           </div>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                           <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                     </div>
                     <!-- /.card-body -->
                  </form:form>
               </div>
               <div id="showCustomer" style="display:none;" class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add Customer</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form:form class="form-horizontal" action="/addCustomer" method="post" modelAttribute="customerDTO">
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="firstname" class="col-sm-3 col-form-label">First Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="firstName" class="form-control" id="firstname" placeholder="First Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="lastname" class="col-sm-3 col-form-label">Last Name</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="lastName" class="form-control" id="lastname" placeholder="Last Name"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="companyname" class="col-sm-4 col-form-label">Office Contact</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="officePhone" class="form-control" id="companyname" placeholder="Office Number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="address" class="col-sm-4 col-form-label">Address</label>
                           <div class="col-sm-10">
                              <input id="location-input" class="form-control" placeholder="Address" maxlength="80" autocomplete="off">
                           </div>
                        </div>
                        <form:hidden path="addressDTO.address1" id="address1" />
                        <form:hidden path="addressDTO.city" id="city" />
                        <form:hidden path="addressDTO.state" id="state" />
                        <form:hidden path="addressDTO.country" id="country" />
                        <form:hidden path="addressDTO.zip" id="zipcode" />
                        <div class="form-group row">
                           <label for="email" class="col-sm-3 col-form-label">Email</label>
                           <div class="col-sm-10">
                              <form:input type="email" path="userDTO.email" class="form-control" id="email" placeholder="EmailId"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="mobile" class="form-control" id="phone" placeholder="Phone number"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="fax" class="col-sm-3 col-form-label">Fax</label>
                           <div class="col-sm-10">
                              <form:input type="text" path="fax" class="form-control" id="fax" placeholder="Fax"/>
                           </div>
                        </div>
                        <div class="form-group row">
                           <div class="offset-sm-2 col-sm-10">
                              <button type="submit" class="btn btn-sm btn-primary">Save</button>
                           </div>
                        </div>
                     </div>
                     <!-- /.card-body -->
                  </form:form>
               </div>
            </div>
            <div class="col-md-3"></div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<script>
   $(document).ready(function() {
   $('#selectCategory').on('change', function() {
   var val = $(this).val();

   if(val == 'employee')
   {
      $('#showEmp').show();
   }else
   {
      $('#showEmp').hide();
   }
   });
   $('#selectCategory').on('change', function() {
          var val = $(this).val();

          if(val == 'vendor')
          {
             $('#showVendor').show();
          }else
          {
             $('#showVendor').hide();
          }
          });

          $('#selectCategory').on('change', function() {
                 var val = $(this).val();

                 if(val == 'customer')
                 {
                    $('#showCustomer').show();
                 }else
                 {
                    $('#showCustomer').hide();
                 }
                 });
           $('#selectCategory').on('change', function() {
                               var val = $(this).val();

                               if(val == 'bank')
                               {
                                  $('#showBank').show();
                               }else
                               {
                                  $('#showBank').hide();
                               }
                               });
   });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6iPEF1Ksldtb4ou_HQLOfL7qammYH_Ds&libraries=places"></script>
<script src="${basePath}js/dist/js/location-map.js"></script>
<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>