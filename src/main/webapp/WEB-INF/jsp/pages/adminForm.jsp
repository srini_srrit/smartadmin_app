<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>

    <div class="content-wrapper" style="min-height: 1200.88px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>General Form</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">General Form</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="form-group row">
                            <label for="input3" class="col-sm-4 col-form-label">Select</label>
                            <div class="col-sm-4">
                                <select id="selectCategory" class="custom-select">
                                    <option value="employee">Employee</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="customer">Customer</option>
                                    <option value="bank">Bank</option>
                                </select>
                            </div>
                        </div>
                        <div id="showEmp" class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Add Employee</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form:form class="form-horizontal" action="${baseUrl}addEmployee" method="post" modelAttribute="employee">
                                <div class="card-body">
                                   <div class="form-group row">
                                        <label for="Email" class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                             <form:input type="text" path="user.username" class="form-control" id="username" placeholder="Email"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="firstname" class="col-sm-4 col-form-label">First Name</label>
                                        <div class="col-sm-10">
                                            <form:input type="text" path="firstName" class="form-control" id="firstname" placeholder="First Name"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-sm-4 col-form-label">Last Name</label>
                                        <div class="col-sm-10">
                                            <form:input type="text" path="lastName" class="form-control" id="lastname" placeholder="Last Name"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Address</label>
                                        <div class="col-sm-10">
                                            <input id="location-input" class="form-control" placeholder="Address" maxlength="80" autocomplete="off">
                                        </div>
                                    </div>
                                    <form:hidden path="address.address1" id="address1" />
                                    <form:hidden path="address.city" id="city" />
                                    <form:hidden path="address.state" id="state" />
                                    <form:hidden path="address.country" id="country" />
                                    <form:hidden path="address.zip" id="zipcode" />
                                    <div class="form-group row">
                                        <label for="email3" class="col-sm-4 col-form-label">E-Mail</label>
                                        <div class="col-sm-10">
                                            <form:input type="email" path="secEmail" class="form-control" id="email3" placeholder="E-mail"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email3" class="col-sm-4 col-form-label">Job Title</label>
                                        <div class="col-sm-10">
                                            <form:input type="text" path="jobTitle" class="form-control" id="jobTitle" placeholder="Job Title"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobile" class="col-sm-4 col-form-label">Phone Number</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="mobile" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobile" class="col-sm-4 col-form-label">About Me</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="aboutMe" placeholder="About Me"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="number" class="col-sm-4 col-form-label">Education Details</label>
                                        <div class="col-sm-10">
                                            <form:textarea class="form-control" path="educationDetails" id="educationDetails" placeholder="Education Details"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="number" class="col-sm-4 col-form-label">Skill Set</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="skillSet" placeholder="Skill Set">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="number" class="col-sm-4 col-form-label">Notes</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="notes" placeholder="Notes"/></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                           <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </form:form>
                        </div>
                        <div id="showBank" class="card card-primary" style="display:none;">
                            <div class="card-header">
                                <h3 class="card-title">Create Bank</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="firstname" class="col-sm-4 col-form-label">Bank Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="bankname" placeholder="Bank Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-sm-4 col-form-label">Contact First Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="firstname" placeholder="First Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-sm-4 col-form-label">Contact Last Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="lastname" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Routing</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="routing" placeholder="Routing">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Account</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="account" placeholder="Account">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Address</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email3" class="col-sm-4 col-form-label">E-Mail</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email3" placeholder="E-mail">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobile" class="col-sm-4 col-form-label">Phone Number</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="phone" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobile" class="col-sm-4 col-form-label">Fax</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="fax" placeholder="Fax">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobile" class="col-sm-4 col-form-label">Website</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="website" placeholder="Website">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobile" class="col-sm-4 col-form-label">About</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="about" placeholder="About"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="number" class="col-sm-4 col-form-label">Notes</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="notes" placeholder="Notes"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                           <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>
                        <div id="showVendor" style="display:none;" class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create Vendor</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="companyname" class="col-sm-4 col-form-label">Company Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="companyname" placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Address</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" placeholder="EmailId">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-4 col-form-label">Phone</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="phone" placeholder="Phone number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fax" class="col-sm-4 col-form-label">Fax</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="fax" placeholder="Fax">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="website" class="col-sm-4 col-form-label">Website</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="website" placeholder="website">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="about" class="col-sm-4 col-form-label">About</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="about" placeholder="enter details"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="associated" class="col-sm-4 col-form-label">Associated Prime Vendors</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="associated" placeholder="enter details">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="notes" class="col-sm-4 col-form-label">Notes</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="notes" placeholder="enter notes"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                     <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                     </div>
                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>
                        <div id="showCustomer" style="display:none;" class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create Customer</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="companyname" class="col-sm-4 col-form-label">Company Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="companyname" placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="firstname" class="col-sm-4 col-form-label">First Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="firstname" placeholder="First Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-sm-4 col-form-label">Last Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="lastname" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Address</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" placeholder="EmailId">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-4 col-form-label">Phone</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="phone" placeholder="Phone number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fax" class="col-sm-4 col-form-label">Fax</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="fax" placeholder="Fax">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="website" class="col-sm-4 col-form-label">Website</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="website" placeholder="website">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="about" class="col-sm-4 col-form-label">About</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="about" placeholder="enter details"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="notes" class="col-sm-4 col-form-label">Notes</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="notes" placeholder="enter notes"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                         <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                         </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>




       <script>
       $(document).ready(function() {
       $('#selectCategory').on('change', function() {
       var val = $(this).val();

       if(val == 'employee')
       {
          $('#showEmp').show();
       }else
       {
          $('#showEmp').hide();
       }
       });
       $('#selectCategory').on('change', function() {
              var val = $(this).val();

              if(val == 'vendor')
              {
                 $('#showVendor').show();
              }else
              {
                 $('#showVendor').hide();
              }
              });

              $('#selectCategory').on('change', function() {
                     var val = $(this).val();

                     if(val == 'customer')
                     {
                        $('#showCustomer').show();
                     }else
                     {
                        $('#showCustomer').hide();
                     }
                     });
               $('#selectCategory').on('change', function() {
                                   var val = $(this).val();

                                   if(val == 'bank')
                                   {
                                      $('#showBank').show();
                                   }else
                                   {
                                      $('#showBank').hide();
                                   }
                                   });
       });
       </script>
       <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6iPEF1Ksldtb4ou_HQLOfL7qammYH_Ds&libraries=places"></script>
       <script src="${basePath}js/dist/js/location-map.js"></script>
    <%@ include file="/WEB-INF/jsp/include/footer.jsp"%>