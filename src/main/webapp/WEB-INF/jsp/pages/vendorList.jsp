<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Vendors</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Vendors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Prime Vendor
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                      <h2 class="lead"><b>Mahaugha LLC</b></h2>
                      <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                     <div class="col-12">
                                          <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                          <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                          <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                          </ul>
                                        </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                     <div class="col-12">
                                          <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                          <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                          <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                          </ul>
                                        </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                                         <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                         <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                         <ul class="ml-4 mb-0 fa-ul text-muted">
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                         </ul>
                                       </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                                         <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                         <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                         <ul class="ml-4 mb-0 fa-ul text-muted">
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                         </ul>
                                       </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                     <div class="col-12">
                                          <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                          <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                          <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                          </ul>
                                        </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                                         <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                         <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                         <ul class="ml-4 mb-0 fa-ul text-muted">
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                         </ul>
                                       </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                     <div class="col-12">
                                          <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                          <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                          <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                          </ul>
                                        </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                                         <h2 class="lead"><b>Mahaugha LLC</b></h2>
                                         <p class="text-muted text-sm"><b>About: </b> Mahaugha LLC is specialized in IT consulting having prime vendorship with Verizon, ATT around New Jersey Area  </p>
                                         <ul class="ml-4 mb-0 fa-ul text-muted">
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span>contact@mahaugha.com</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> +1 (800) 121-2232</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-map-marker-alt"></i></span> Demo Street 123, Demo City 04312, NJ</li>
                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span>www.mahaugha.com</li>
                                         </ul>
                                       </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments"></i>
                    </a>
                    <a href="${baseUrl}vendorProfile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul class="pagination justify-content-center m-0">
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">4</a></li>
              <li class="page-item"><a class="page-link" href="#">5</a></li>
              <li class="page-item"><a class="page-link" href="#">6</a></li>
              <li class="page-item"><a class="page-link" href="#">7</a></li>
              <li class="page-item"><a class="page-link" href="#">8</a></li>
            </ul>
          </nav>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>