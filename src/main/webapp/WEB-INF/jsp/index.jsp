<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>

 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <div class="content-header">
       <div class="container-fluid">
         <div class="row mb-2">
           <div class="col-sm-6">
             <h1 class="m-0 text-dark">Srrit Solutions</h1>
           </div><!-- /.col -->
           <div class="col-sm-6">
             <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="#">Home</a></li>
               <li class="breadcrumb-item active">Dashboard</li>
             </ol>
           </div><!-- /.col -->
         </div><!-- /.row -->
       </div><!-- /.container-fluid -->
     </div>
     <!-- /.content-header -->

     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <!-- Info boxes -->
         <div class="row">
           <div class="col-12 col-sm-6 col-md-3">
             <div class="info-box">
               <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-tie"></i></span>
               <div class="info-box-content">
                 <span class="info-box-text">Employees</span>
                 <span class="info-box-number">
                   1080
                 </span>
               </div>
               <!-- /.info-box-content -->
             </div>
             <!-- /.info-box -->
           </div>
           <!-- /.col -->
           <div class="col-12 col-sm-6 col-md-3">
             <div class="info-box mb-3">
               <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-store-alt"></i></span>
               <div class="info-box-content">
                 <span class="info-box-text">Vendors</span>
                 <span class="info-box-number">41,410</span>
               </div>
               <!-- /.info-box-content -->
             </div>
             <!-- /.info-box -->
           </div>
           <!-- /.col -->

           <!-- fix for small devices only -->
           <div class="clearfix hidden-md-up"></div>

           <div class="col-12 col-sm-6 col-md-3">
             <div class="info-box mb-3">
               <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
               <div class="info-box-content">
                 <span class="info-box-text">Customers</span>
                 <span class="info-box-number">760</span>
               </div>
               <!-- /.info-box-content -->
             </div>
             <!-- /.info-box -->
           </div>
           <!-- /.col -->
           <div class="col-12 col-sm-6 col-md-3">
             <div class="info-box mb-3">
               <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-envelope"></i></span>

               <div class="info-box-content">
                 <span class="info-box-text">Notifications</span>
                 <span class="info-box-number">2,000</span>
               </div>
               <!-- /.info-box-content -->
             </div>
             <!-- /.info-box -->
           </div>
           <!-- /.col -->
         </div>
         <!-- /.row -->

         <div class="row">
           <div class="col-md-12">
             <div class="card">
               <div class="card-header">
                 <h5 class="card-title">Monthly Recap Report</h5>

                 <div class="card-tools">
                   <button type="button" class="btn btn-tool" data-card-widget="collapse">
                     <i class="fas fa-minus"></i>
                   </button>
                   <div class="btn-group">
                     <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                       <i class="fas fa-wrench"></i>
                     </button>
                     <div class="dropdown-menu dropdown-menu-right" role="menu">
                       <a href="#" class="dropdown-item">Action</a>
                       <a href="#" class="dropdown-item">Another action</a>
                       <a href="#" class="dropdown-item">Something else here</a>
                       <a class="dropdown-divider"></a>
                       <a href="#" class="dropdown-item">Separated link</a>
                     </div>
                   </div>
                   <button type="button" class="btn btn-tool" data-card-widget="remove">
                     <i class="fas fa-times"></i>
                   </button>
                 </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                 <div class="row">
                   <div class="col-md-8">
                     <p class="text-center">
                       <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                     </p>

                     <div class="chart">
                       <!-- Sales Chart Canvas -->
                       <canvas id="salesChart" height="180" style="height: 180px;"></canvas>
                     </div>
                     <!-- /.chart-responsive -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-4">
                     <p class="text-center">
                       <strong>Goal Completion</strong>
                     </p>

                     <div class="progress-group">
                       Add Products to Cart
                       <span class="float-right"><b>160</b>/200</span>
                       <div class="progress progress-sm">
                         <div class="progress-bar bg-primary" style="width: 80%"></div>
                       </div>
                     </div>
                     <!-- /.progress-group -->

                     <div class="progress-group">
                       Complete Purchase
                       <span class="float-right"><b>310</b>/400</span>
                       <div class="progress progress-sm">
                         <div class="progress-bar bg-danger" style="width: 75%"></div>
                       </div>
                     </div>

                     <!-- /.progress-group -->
                     <div class="progress-group">
                       <span class="progress-text">Visit Premium Page</span>
                       <span class="float-right"><b>480</b>/800</span>
                       <div class="progress progress-sm">
                         <div class="progress-bar bg-success" style="width: 60%"></div>
                       </div>
                     </div>

                     <!-- /.progress-group -->
                     <div class="progress-group">
                       Send Inquiries
                       <span class="float-right"><b>250</b>/500</span>
                       <div class="progress progress-sm">
                         <div class="progress-bar bg-warning" style="width: 50%"></div>
                       </div>
                     </div>
                     <!-- /.progress-group -->
                   </div>
                   <!-- /.col -->
                 </div>
                 <!-- /.row -->
               </div>
               <!-- ./card-body -->
               <div class="card-footer">
                 <div class="row">
                   <div class="col-sm-4 col-6">
                     <div class="description-block border-right">
                       <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span>
                       <h5 class="description-header">$35,210.43</h5>
                       <span class="description-text">TOTAL REVENUE</span>
                     </div>
                     <!-- /.description-block -->
                   </div>
                   <!-- /.col -->
                   <div class="col-sm-4 col-6">
                     <div class="description-block border-right">
                       <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span>
                       <h5 class="description-header">$10,390.90</h5>
                       <span class="description-text">TOTAL COST</span>
                     </div>
                     <!-- /.description-block -->
                   </div>
                   <!-- /.col -->
                   <div class="col-sm-4 col-6">
                     <div class="description-block border-right">
                       <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 20%</span>
                       <h5 class="description-header">$24,813.53</h5>
                       <span class="description-text">TOTAL PROFIT</span>
                     </div>
                     <!-- /.description-block -->
                   </div>
                   <!-- /.col -->
                   <div class="col-sm-3 col-6">
                     <div class="description-block">
                       <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
                       <h5 class="description-header">1200</h5>
                       <span class="description-text">GOAL COMPLETIONS</span>
                     </div>
                     <!-- /.description-block -->
                   </div>
                 </div>
                 <!-- /.row -->
               </div>
               <!-- /.card-footer -->
             </div>
             <!-- /.card -->
           </div>
           <!-- /.col -->
         </div>
         <!-- /.row -->

         <!-- Main row -->
         <div class="row">
           <!-- Left col -->
           <div class="col-md-8">
             <!-- TABLE: LATEST ORDERS -->
             <div class="card">
               <div class="card-header border-transparent">
                 <h3 class="card-title">Invoices</h3>

                 <div class="card-tools">
                   <button type="button" class="btn btn-tool" data-card-widget="collapse">
                     <i class="fas fa-minus"></i>
                   </button>
                   <button type="button" class="btn btn-tool" data-card-widget="remove">
                     <i class="fas fa-times"></i>
                   </button>
                 </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body p-0">
                 <div class="table-responsive">
                   <table class="table m-0">
                     <thead>
                     <tr>
                       <th>Invoice ID</th>
                       <th>Item</th>
                       <th>Status</th>
                       <th>SubTotal</th>
                     </tr>
                     </thead>
                     <tbody>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR9842</a></td>
                       <td>Call of Duty IV</td>
                       <td><span class="badge badge-success">Shipped</span></td>
                       <td>
                         <div class="sparkbar" data-color="#00a65a" data-height="20">$1023</div>
                       </td>
                     </tr>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR1848</a></td>
                       <td>Samsung Smart TV</td>
                       <td><span class="badge badge-warning">Pending</span></td>
                       <td>
                         <div class="sparkbar" data-color="#f39c12" data-height="20">$1023</div>
                       </td>
                     </tr>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR7429</a></td>
                       <td>iPhone 6 Plus</td>
                       <td><span class="badge badge-danger">Delivered</span></td>
                       <td>
                         <div class="sparkbar" data-color="#f56954" data-height="20">$1023</div>
                       </td>
                     </tr>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR7429</a></td>
                       <td>Samsung Smart TV</td>
                       <td><span class="badge badge-info">Processing</span></td>
                       <td>
                         <div class="sparkbar" data-color="#00c0ef" data-height="20">$1023</div>
                       </td>
                     </tr>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR1848</a></td>
                       <td>Samsung Smart TV</td>
                       <td><span class="badge badge-warning">Pending</span></td>
                       <td>
                         <div class="sparkbar" data-color="#f39c12" data-height="20">$1023</div>
                       </td>
                     </tr>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR7429</a></td>
                       <td>iPhone 6 Plus</td>
                       <td><span class="badge badge-danger">Delivered</span></td>
                       <td>
                         <div class="sparkbar" data-color="#f56954" data-height="20">$1023</div>
                       </td>
                     </tr>
                     <tr>
                       <td><a href="pages/examples/invoice.html">OR9842</a></td>
                       <td>Call of Duty IV</td>
                       <td><span class="badge badge-success">Shipped</span></td>
                       <td>
                         <div class="sparkbar" data-color="#00a65a" data-height="20">$1023</div>
                       </td>
                     </tr>
                     </tbody>
                   </table>
                 </div>
                 <!-- /.table-responsive -->
               </div>
               <!-- /.card-body -->
               <div class="card-footer clearfix">
                 <a href="javascript:void(0)" class="btn btn-sm btn-primary float-left">Create New Invoice</a>
                 <a href="javascript:void(0)" class="btn btn-sm btn-primary float-right">View All Invoices</a>
               </div>
               <!-- /.card-footer -->
             </div>
             <!-- /.card -->
           </div>
           <!-- /.col -->

           <div class="col-md-4">
             <div class="d-none card">
               <div class="card-header">
                 <h3 class="card-title">Browser Usage</h3>
                 <div class="card-tools">
                   <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                   </button>
                   <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                   </button>
                 </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                 <div class="row">
                   <div class="col-md-8">
                     <div class="chart-responsive">
                       <canvas id="pieChart" height="150"></canvas>
                     </div>
                     <!-- ./chart-responsive -->
                   </div>
                   <!-- /.col -->
                   <div class="col-md-4">
                     <ul class="chart-legend clearfix">
                       <li><i class="far fa-circle text-danger"></i> Chrome</li>
                       <li><i class="far fa-circle text-success"></i> IE</li>
                       <li><i class="far fa-circle text-warning"></i> FireFox</li>
                       <li><i class="far fa-circle text-info"></i> Safari</li>
                       <li><i class="far fa-circle text-primary"></i> Opera</li>
                       <li><i class="far fa-circle text-secondary"></i> Navigator</li>
                     </ul>
                   </div>
                   <!-- /.col -->
                 </div>
                 <!-- /.row -->
               </div>
               <!-- /.card-body -->
               <div class="card-footer bg-white p-0">
                 <ul class="nav nav-pills flex-column">
                   <li class="nav-item">
                     <a href="#" class="nav-link">
                       United States of America
                       <span class="float-right text-danger">
                         <i class="fas fa-arrow-down text-sm"></i>
                         12%</span>
                     </a>
                   </li>
                   <li class="nav-item">
                     <a href="#" class="nav-link">
                       India
                       <span class="float-right text-success">
                         <i class="fas fa-arrow-up text-sm"></i> 4%
                       </span>
                     </a>
                   </li>
                   <li class="nav-item">
                     <a href="#" class="nav-link">
                       China
                       <span class="float-right text-warning">
                         <i class="fas fa-arrow-left text-sm"></i> 0%
                       </span>
                     </a>
                   </li>
                 </ul>
               </div>
               <!-- /.footer -->
             </div>
             <div class="card">
               <div class="card-header">
                 <h3 class="card-title">Latest Contacts</h3>

                 <div class="card-tools">
                   <span class="badge badge-danger">8 New Contacts</span>
                   <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                   </button>
                   <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                   </button>
                 </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body p-0">
                 <ul class="users-list clearfix">
                   <li>
                     <img src="${basePath}img/user1-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Alexander Pierce</a>
                     <span class="users-list-date">Today</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user8-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Norman</a>
                     <span class="users-list-date">Yesterday</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user7-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Jane</a>
                     <span class="users-list-date">12 Jan</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user6-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">John</a>
                     <span class="users-list-date">12 Jan</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user2-160x160.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Alexander</a>
                     <span class="users-list-date">13 Jan</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user5-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Sarah</a>
                     <span class="users-list-date">14 Jan</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user4-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Nora</a>
                     <span class="users-list-date">15 Jan</span>
                   </li>
                   <li>
                     <img src="${basePath}img/user3-128x128.jpg" alt="User Image">
                     <a class="users-list-name" href="#">Nadia</a>
                     <span class="users-list-date">15 Jan</span>
                   </li>
                 </ul>
                 <!-- /.users-list -->
               </div>
               <!-- /.card-body -->
               <div class="card-footer text-center">
                 <a href="javascript::">View All Contacts</a>
               </div>
               <!-- /.card-footer -->
             </div>
           </div>
           <!-- /.col -->
         </div>
         <!-- /.row -->
       </div><!--/. container-fluid -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->

   <!-- Control Sidebar -->
   <aside class="control-sidebar control-sidebar-dark">
     <!-- Control sidebar content goes here -->
   </aside>

   <!-- /.control-sidebar -->

   <script src="${basePath}js/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
   <script src="${basePath}js/plugins/raphael/raphael.min.js"></script>
   <script src="${basePath}js/plugins/jquery-mapael/jquery.mapael.min.js"></script>
   <script src="${basePath}js/plugins/jquery-mapael/maps/usa_states.min.js"></script>
   <!-- PAGE SCRIPTS -->
   <script src="${basePath}js/dist/js/pages/dashboard2.js"></script>

<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>