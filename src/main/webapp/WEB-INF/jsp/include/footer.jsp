<%@ include file="/WEB-INF/jsp/include/includes.jsp"%>
    <footer class="main-footer">
      <div class="row">
              <div class="col-sm-7">
               | <a href="/aboutus">About Us</a> |
                 <a href="/faqs">FAQs</a> |
              <a href="/terms">Terms</a> |
              <a href="/privacy">Privacy</a> |
              </div>
                <div class="col-sm-5">
                  <strong>&copy 2019  <a href="http://www.srritsolutions.com">srritsolutions.com</a>. All rights reserved</strong>
             </div>
            </div>
            </footer>


      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->

    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="${basePath}js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="${basePath}js/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="${basePath}js/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="${basePath}js/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="${basePath}js/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="${basePath}js/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="${basePath}js/plugins/moment/moment.min.js"></script>
    <script src="${basePath}js/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="${basePath}js/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="${basePath}js/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="${basePath}js/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="${basePath}js/dist/js/adminlte.js"></script>

    </body>
    </html>
