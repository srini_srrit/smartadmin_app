<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>

    <div class="content-wrapper" style="min-height: 1200.88px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Contact Form</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Contact Form</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Contact Us</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form:form method="post" name="contactusform" id="contactusform"
                            					modelAttribute="contact" action="contact-us">
                            <input type="hidden" name="${_csrf.parameterName}"
                            								value="${_csrf.token}" />
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="firstname" class="col-sm-4 col-form-label">Your Name</label>
                                        <div class="col-sm-10">
                                            	<form:input path="name" id="name" class="form-control" placeholder="Your Name *" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                           	<form:input path="email" id="email"  class="form-control" placeholder="Email *" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-sm-4 col-form-label">Subject</label>
                                        <div class="col-sm-10">
                                           <form:input path="subject" id="subject"
                                           									class="form-control" placeholder="Subject *" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label">Your message</label>
                                        <div class="col-sm-10">
                                         	<form:textarea path="message" name="message" cols="50" rows="6"  class="form-control" placeholder="Your message *" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-1 col-sm-10">
                                           <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </form:form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <%@ include file="/WEB-INF/jsp/include/footer.jsp"%>