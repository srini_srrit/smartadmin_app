<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
<div class="content-wrapper">

<section class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Employees</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Create Admin</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container">
      <div class="row">
          <div class="col-lg-12">
              <form id="rendered-form">
                 <div class="rendered-form">
                    <div class="">
                       <h1 id="control-1641490">Bank Registration</h1>
                    </div>
                    <div class="fb-text form-group field-text-1576817069174"><label for="text-1576817069174" class="fb-text-label">Bank Name</label><input type="text" class="form-control" name="text-1576817069174" id="text-1576817069174"></div>
                    <div class="fb-text form-group field-text-1576817587603"><label for="text-1576817587603" class="fb-text-label">Contact First Name</label><input type="text" class="form-control" name="text-1576817587603" id="text-1576817587603"></div>
                    <div class="fb-text form-group field-text-1576817682843"><label for="text-1576817682843" class="fb-text-label">Contact Last Name</label><input type="text" class="form-control" name="text-1576817682843" id="text-1576817682843"></div>
                    <div class="fb-text form-group field-text-1576817712486"><label for="text-1576817712486" class="fb-text-label">Routing</label><input type="text" class="form-control" name="text-1576817712486" id="text-1576817712486"></div>
                    <div class="fb-text form-group field-text-1576817720658"><label for="text-1576817720658" class="fb-text-label">Account</label><input type="text" class="form-control" name="text-1576817720658" id="text-1576817720658"></div>
                    <div class="fb-text form-group field-text-1576817096799"><label for="text-1576817096799" class="fb-text-label">Address</label><input type="text" class="form-control" name="text-1576817096799" id="text-1576817096799"></div>
                    <div class="fb-text form-group field-text-1576817099105"><label for="text-1576817099105" class="fb-text-label">Email</label><input type="text" class="form-control" name="text-1576817099105" id="text-1576817099105"></div>
                    <div class="fb-text form-group field-text-1576817288370"><label for="text-1576817288370" class="fb-text-label">Phone</label><input type="text" class="form-control" name="text-1576817288370" id="text-1576817288370"></div>
                    <div class="fb-text form-group field-text-1576817275832"><label for="text-1576817275832" class="fb-text-label">Fax</label><input type="text" class="form-control" name="text-1576817275832" id="text-1576817275832"></div>
                    <div class="fb-text form-group field-text-1576817300445"><label for="text-1576817300445" class="fb-text-label">Website</label><input type="text" class="form-control" name="text-1576817300445" id="text-1576817300445"></div>
                    <div class="fb-text form-group field-text-1576817524297"><label for="text-1576817524297" class="fb-text-label">About</label><input type="text" class="form-control" name="text-1576817524297" id="text-1576817524297"></div>
                    <div class="fb-text form-group field-text-1576817351419"><label for="text-1576817351419" class="fb-text-label">Notes</label><input type="text" class="form-control" name="text-1576817351419" id="text-1576817351419"></div>
                    <div class="fb-button form-group field-button-1576817426021"><button type="button" class="btn-default btn" name="button-1576817426021" style="default" id="button-1576817426021">Save</button></div>
                 </div>
              </form>
          </div>
      </div>
   </div>
 </section>
 </div>

<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>