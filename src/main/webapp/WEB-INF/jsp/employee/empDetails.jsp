<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Profile</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">User Profile</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-3">
               <!-- Profile Image -->
               <div class="card card-primary card-outline">
                  <div class="card-body box-profile">
                     <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                           src="${basePath}img/user4-128x128.jpg"
                           alt="User profile picture">
                     </div>
                     <h3 class="profile-username text-center">${empDetails.firstName} ${empDetails.lastName}</h3>
                     <p class="text-muted text-center">${empDetails.jobTitle}</p>
                     <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                           <b>Id</b> <a class="float-right">${empDetails.employeeId}</a>
                        </li>
                        <li class="list-group-item">
                           <b>Phone</b> <a class="float-right">${empDetails.phone}</a>
                        </li>
                        <li class="list-group-item">
                           <b>Email</b> <a class="float-right">${empDetails.user.email}</a>
                        </li>
                     </ul>
                  </div>
                  <!-- /.card-body -->
               </div>
               <!-- /.card -->
               <!-- About Me Box -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">About Me</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                     <strong><i class="fas fa-book mr-1"></i> Education</strong>
                     <p class="text-muted">
                        B.S. in Computer Science from the University of Tennessee at Knoxville
                     </p>
                     <hr>
                     <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                     <p class="text-muted">Malibu, California</p>
                     <hr>
                     <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>
                     <p class="text-muted">
                        <span class="tag tag-danger">UI Design</span>
                        <span class="tag tag-success">Coding</span>
                        <span class="tag tag-info">Javascript</span>
                        <span class="tag tag-warning">PHP</span>
                        <span class="tag tag-primary">Node.js</span>
                     </p>
                     <hr>
                     <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>
                     <p class="text-muted">Looking for a new job in UI Design.</p>
                  </div>
                  <!-- /.card-body -->
               </div>
               <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
               <div class="card">
                  <div class="card-header p-2">
                     <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">TimeSheet</a></li>
                        <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                     </ul>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                           <!-- Post -->
                           <div class="post">
                              <div class="user-block">
                                 <img class="img-circle img-bordered-sm" src="${basePath}img/user1-128x128.jpg" alt="user image">
                                 <span class="username">
                                 <a href="#">Jonathan Burke Jr.</a>
                                 <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                 </span>
                                 <span class="description">Shared publicly - 7:30 PM today</span>
                              </div>
                              <!-- /.user-block -->
                              <p>
                                 Lorem ipsum represents a long-held tradition for designers,
                                 typographers and the like. Some people hate it and argue for
                                 its demise, but others ignore the hate as they create awesome
                                 tools to help create filler text for everyone from bacon lovers
                                 to Charlie Sheen fans.
                              </p>
                              <p>
                                 <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                 <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                 <span class="float-right">
                                 <a href="#" class="link-black text-sm">
                                 <i class="far fa-comments mr-1"></i> Comments (5)
                                 </a>
                                 </span>
                              </p>
                              <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                           </div>
                           <!-- /.post -->
                           <!-- Post -->
                           <div class="post clearfix">
                              <div class="user-block">
                                 <img class="img-circle img-bordered-sm" src="${basePath}img/user7-128x128.jpg" alt="User Image">
                                 <span class="username">
                                 <a href="#">Sarah Ross</a>
                                 <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                 </span>
                                 <span class="description">Sent you a message - 3 days ago</span>
                              </div>
                              <!-- /.user-block -->
                              <p>
                                 Lorem ipsum represents a long-held tradition for designers,
                                 typographers and the like. Some people hate it and argue for
                                 its demise, but others ignore the hate as they create awesome
                                 tools to help create filler text for everyone from bacon lovers
                                 to Charlie Sheen fans.
                              </p>
                              <form class="form-horizontal">
                                 <div class="input-group input-group-sm mb-0">
                                    <input class="form-control form-control-sm" placeholder="Response">
                                    <div class="input-group-append">
                                       <button type="submit" class="btn btn-primary">Send</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           <!-- /.post -->
                           <!-- Post -->
                           <div class="post">
                              <div class="user-block">
                                 <img class="img-circle img-bordered-sm" src="${basePath}img/user6-128x128.jpg" alt="User Image">
                                 <span class="username">
                                 <a href="#">Adam Jones</a>
                                 <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                 </span>
                                 <span class="description">Posted 5 photos - 5 days ago</span>
                              </div>
                              <!-- /.user-block -->
                              <div class="row mb-3">
                                 <div class="col-sm-6">
                                    <img class="img-fluid" src="${basePath}img/photo1.png" alt="Photo">
                                 </div>
                                 <!-- /.col -->
                                 <div class="col-sm-6">
                                    <div class="row">
                                       <div class="col-sm-6">
                                          <img class="img-fluid mb-3" src="${basePath}img/photo2.png" alt="Photo">
                                          <img class="img-fluid" src="${basePath}img/photo3.jpg" alt="Photo">
                                       </div>
                                       <!-- /.col -->
                                       <div class="col-sm-6">
                                          <img class="img-fluid mb-3" src="${basePath}img/photo4.jpg" alt="Photo">
                                          <img class="img-fluid" src="${basePath}img/photo1.png" alt="Photo">
                                       </div>
                                       <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                 </div>
                                 <!-- /.col -->
                              </div>
                              <!-- /.row -->
                              <p>
                                 <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                 <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                 <span class="float-right">
                                 <a href="#" class="link-black text-sm">
                                 <i class="far fa-comments mr-1"></i> Comments (5)
                                 </a>
                                 </span>
                              </p>
                              <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                           </div>
                           <!-- /.post -->
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="timeline">
                           <div class="row">
                              <div class="col-12">
                                 <div class="card">
                                    <div class="card-header">
                                       <h3 class="card-title">Employee TimeSheet</h3>
                                       <div class="card-tools">
                                          <div class="input-group input-group-sm" style="width: 150px;">
                                             <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                             <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive p-0" style="height: 300px;">
                                       <table class="table table-head-fixed">
                                          <thead>
                                             <tr>
                                                <th>ProjectID</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Location</th>
                                                <th>Hours</th>
                                                <th>status</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr>
                                                <td>183</td>
                                                <td>11-7-2018</td>
                                                <td>12-8-2019</td>
                                                <td>East Windsor, NJ 08512, USA.</td>
                                                <td>9</td>
                                                <td><span class="tag tag-success">Approved</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>219</td>
                                                <td>01-17-2016</td>
                                                <td>11-7-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>8</td>
                                                <td><span class="tag tag-warning">Pending</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>657</td>
                                                <td>11-27-2018</td>
                                                <td>10-11-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>9</td>
                                                <td><span class="tag tag-primary">Approved</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>175</td>
                                                <td>01-17-2016</td>
                                                <td>11-7-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>10</td>
                                                <td><span class="tag tag-danger">Denied</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>134</td>
                                                <td>11-27-2018</td>
                                                <td>10-11-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>6</td>
                                                <td><span class="tag tag-success">Approved</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>494</td>
                                                <td>11-27-2018</td>
                                                <td>10-11-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>9</td>
                                                <td><span class="tag tag-warning">Pending</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>832</td>
                                                <td>01-17-2016</td>
                                                <td>11-7-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>7</td>
                                                <td><span class="tag tag-primary">Approved</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>982</td>
                                                <td>11-27-2018</td>
                                                <td>10-11-2019</td>
                                                <td>East Windsor, NJ 08512, USA</td>
                                                <td>8</td>
                                                <td><span class="tag tag-danger">Denied</span></td>
                                                <td>
                                                   <div class="btn-group btn-group-sm">
                                                      <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                                      <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                   </div>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <!-- /.card-body -->
                                 </div>
                                 <!-- /.card -->
                              </div>
                           </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="settings">
                           <form:form class="form-horizontal" method="post" action="updateEmployee" modelAttribute="empDetails">
                              <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">First Name</label>
                                 <div class="col-sm-10">
                                    <form:input type="text" path="firstName" class="form-control" id="inputName" placeholder="Name"/>
                                 </div>
                              </div>
                              <div class="form-group row">
                                  <label for="inputName" class="col-sm-2 col-form-label">Last Name</label>
                                  <div class="col-sm-10">
                                    <form:input type="text" path="lastName" class="form-control" id="inputName" placeholder="Name"/>
                                  </div>
                              </div>
                              <div class="form-group row">
                                 <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                 <div class="col-sm-10">
                                    <form:input type="email" path="secEmail" class="form-control" id="inputEmail" placeholder="Email"/>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="inputName2" class="col-sm-2 col-form-label">Education</label>
                                 <div class="col-sm-10">
                                    <form:input type="text" path="educationDetails" class="form-control" id="inputName2" placeholder="Education"/>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                                 <div class="col-sm-10">
                                    <form:input type="text" path="skills" class="form-control" id="inputSkills" placeholder="Skills"/>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-sm btn-primary">Update Profile</button>
                                 </div>
                              </div>
                           </form:form>
                        </div>
                        <!-- /.tab-pane -->
                     </div>
                     <!-- /.tab-content -->
                  </div>
                  <!-- /.card-body -->
               </div>
               <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>