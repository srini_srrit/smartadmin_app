<html>
	<body>  
		<h4>Dear ${email.user.firstName},
		<br/>
		</h4>
		<div>		
			Thank you for registering with NRI Wala. We sincerely hope that you will find all your community needs and savings at <a href="${email.siteUrl}">Nriwala.com</a>.
			<br/><br/>		
			Please click on the link below to verify your account.<br/><br/>
			 <a href="${email.accountVerifyLink}">Activate your account</a> <br/><br/>
			Or copy and paste the URL into your browser:<br/>${email.accountVerifyLink}
			<br/><br/><br/><br/>
  			For any further questions please contact us at <a href="mailto:${email.emailFrom}">${email.emailFrom}</a> or you can submit your questions <a href="${email.contactUsLink}">here</a>.
  		 	<br/><br/><br/>
		Thank you,<br/>
		TechAdminpro.com Team<br/>
		<br/><br/>
		</div>
	</body>
</html>