<html>
	<body> 
	 
	<h4>Dear ${email.user.firstName}, 
		<br/>
		</h4>
		<div> 
			We are sending this email to let you know that your profile at Nriwala.com has been updated.<br/><br/>		
		
			For further questions please contact us at <a href="mailto:${email.emailFrom}">${email.emailFrom}</a> or you can submit your questions <a href="${email.contactUsLink}"> here</a>.
   	
			<br/><br/>	<br/> 
			Thank you,<br/>
			TechAdminpro.com Team<br/> 	 	<br/><br/>
		</div>
	</body>
</html>