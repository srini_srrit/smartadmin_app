<html>
	<body>
	 
		<h4>Dear ${email.user.firstName}, 
		<br/>
		</h4
		<div>		
			To set your new password, please click the link below:
			<br/><br/>		
			<a href="${email.accountResetLink}">Reset password here</a><br/><br/><br/>
			Note: If you don't use this link within 24 hours, it will expire. To get a new password reset link, visit <a href="${email.passwordResetLink}">${email.passwordResetLink}</a><br/><br/> 
  		 	
  		 	For any further questions please contact us at <a href="mailto:${email.emailFrom}">${email.emailFrom}</a> or you can submit your questions <a href="${email.contactUsLink}"> here</a>.
  		 	<br/><br/><br/>
		Thank you,<br/>
	TechAdminpro.com Team<br/>
		<br/>
		</div>
	</body>
</html>