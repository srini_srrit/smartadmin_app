-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 52.73.210.112    Database: smartadmin_app
-- ------------------------------------------------------
-- Server version	5.5.62

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `ADDRESS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS1` varchar(60) DEFAULT NULL,
  `ADDRESS2` varchar(45) DEFAULT NULL,
  `ADDRESS3` varchar(45) DEFAULT NULL,
  `CITY` varchar(45) DEFAULT NULL,
  `STATE` varchar(45) DEFAULT NULL,
  `ZIP` varchar(10) DEFAULT NULL,
  `COUNTRY` varchar(2) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ADDRESS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (238,'',NULL,NULL,'','','','US','2019-12-21 09:06:25'),(240,'',NULL,NULL,'','','','US','2019-12-21 09:06:25'),(242,'',NULL,NULL,NULL,'','','US','2019-12-21 09:06:25'),(244,'',NULL,NULL,NULL,'','','US','2019-12-21 09:06:25'),(246,'',NULL,NULL,NULL,'','','US','2019-12-21 09:06:25'),(248,'',NULL,NULL,'','','','US','2019-12-21 10:22:15'),(250,'',NULL,NULL,'','','','US','2019-12-21 15:23:09'),(261,'709 Forest View Dr',NULL,NULL,'Avenel','NJ','07001',NULL,'2019-12-21 16:41:13');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banks` (
  `BANK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BANK_NAME` varchar(60) DEFAULT NULL,
  `BANK_TYPE` varchar(10) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `FAX` varchar(15) DEFAULT NULL,
  `CONTACT_NAME` varchar(45) DEFAULT NULL,
  `ADDRESS_ID` int(11) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`BANK_ID`),
  KEY `bank_address_fk_key_idx` (`ADDRESS_ID`),
  CONSTRAINT `bank_address_fk_key_idx` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ADDRESS_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `CUSTOMER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(60) DEFAULT NULL,
  `LAST_NAME` varchar(60) DEFAULT NULL,
  `MOBILE` varchar(15) DEFAULT NULL,
  `OFFICE_PHONE` varchar(15) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `SEC_EMAIL` varchar(60) DEFAULT NULL,
  `FAX` varchar(15) DEFAULT NULL,
  `CONTACT_NAME` varchar(45) DEFAULT NULL,
  `ADDRESS_ID` int(11) DEFAULT NULL,
  `BANK_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CUSTOMER_ID`),
  KEY `customer_bank_fk_key_idx` (`BANK_ID`),
  KEY `customer_user_fk_key_idx` (`USER_ID`),
  KEY `customer_address_fk_key_idx` (`ADDRESS_ID`),
  CONSTRAINT `customer_address_fk_key_idx` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ADDRESS_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `customer_bank_fk_key` FOREIGN KEY (`BANK_ID`) REFERENCES `banks` (`BANK_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `customer_user_fk_key_idx` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (241,'Bhagath','kumar','8099294490',NULL,NULL,'bhagathkumar4490@gmail.com','8099294490',NULL,242,NULL,NULL,'2019-12-21 08:51:50'),(243,'vivek','reddy','8309248372',NULL,1,'vivekreddy384@gmail.com','8309248372',NULL,244,NULL,NULL,'2019-12-21 08:51:50'),(245,'Bhagath','kumar','8099294490',NULL,1,'bhagathkumar4490@gmail.com','8099294490',NULL,246,NULL,NULL,'2019-12-21 08:52:11');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `EMPLOYEE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(60) DEFAULT NULL,
  `LAST_NAME` varchar(60) DEFAULT NULL,
  `SEX` char(1) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `SEC_EMAIL` varchar(60) DEFAULT NULL,
  `ADDRESS_ID` int(11) DEFAULT NULL,
  `BANK_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `EDUCATION_DETAILS` varchar(200) DEFAULT NULL,
  `PHONE` varchar(45) DEFAULT NULL,
  `JOB_TITLE` varchar(100) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`EMPLOYEE_ID`),
  KEY `employee_address_fk_key_idx` (`ADDRESS_ID`),
  KEY `employee_bank_fk_key_idx` (`BANK_ID`),
  KEY `employee_user_fk_key_idx` (`USER_ID`),
  CONSTRAINT `employee_address_fk_key` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ADDRESS_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `employee_bank_fk_key` FOREIGN KEY (`BANK_ID`) REFERENCES `banks` (`BANK_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `employee_user_fk_key_idx` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (237,'bhagath','kumar','\0',0,'bhagath4490@gmail.com',238,NULL,NULL,'testing','','Testing','2019-12-09 04:18:15'),(239,'raju','kumar','\0',0,'bhagathkumar4490@gmail.com',240,NULL,NULL,'completed phd','','analyst','2019-12-21 09:07:01'),(247,'manoj','kumar','\0',0,'bhagath4490@gmail.com',248,NULL,NULL,'completed phd ','','Software Adviser','2019-12-21 10:22:16'),(249,'sai ','kiran','\0',1,'chari439@gmail.com',250,NULL,NULL,'Post Graduation','8099294490','analyst','2019-12-21 15:23:09'),(260,'Srini','K','\0',1,'kpsvas@gmail.com',261,NULL,262,'Master in Computer Science','1234512333','System Admin','2019-12-21 16:42:36'),(261,'',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-12-21 16:42:16');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (263),(263);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_details`
--

DROP TABLE IF EXISTS `invoice_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_details` (
  `INVOICES_DETAIL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INVOICE_ID` int(11) DEFAULT NULL,
  `ATTCHMENT_COUNT` int(2) DEFAULT NULL,
  `KEYWORDS` varchar(200) DEFAULT NULL,
  `TAGS` varchar(300) DEFAULT NULL,
  `ATTACHMENT_1` blob,
  `ATTACHMENT_2` blob,
  `ATTACHMENT_3` blob,
  `ATTACHMENT_4` blob,
  `ATTACHMENT_5` blob,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`INVOICES_DETAIL_ID`),
  KEY `invoicedetail_invoice_fk_key_idx` (`INVOICE_ID`),
  CONSTRAINT `invoicedetail_invoice_fk_key_idx` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoices` (`INVOICE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_details`
--

LOCK TABLES `invoice_details` WRITE;
/*!40000 ALTER TABLE `invoice_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `INVOICE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(45) DEFAULT NULL,
  `TITLE` varchar(60) DEFAULT NULL,
  `INVOICE_TYPE` varchar(15) DEFAULT NULL,
  `INVOICE_DATE` date DEFAULT NULL,
  `DUE_DATE` date DEFAULT NULL,
  `PAID_DATE` date DEFAULT NULL,
  `AMOUNT` decimal(8,2) DEFAULT NULL,
  `DESC` varchar(200) DEFAULT NULL,
  `ADD_DETAILS` varchar(300) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `VENDOR_ID` int(11) DEFAULT NULL,
  `PROJECT_ID` int(11) DEFAULT NULL,
  `TIMESHEET_ID` int(11) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`INVOICE_ID`),
  KEY `invoice_vendor_fk_key_idx` (`VENDOR_ID`),
  KEY `invoice_project_fk_key_idx` (`PROJECT_ID`),
  KEY `invoice_timesheet_fk_key_idx` (`TIMESHEET_ID`),
  CONSTRAINT `invoice_project_fk_key_idx` FOREIGN KEY (`PROJECT_ID`) REFERENCES `projects` (`PROJECT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `invoice_timesheet_fk_key_idx` FOREIGN KEY (`TIMESHEET_ID`) REFERENCES `timesheets` (`TIMESHEET_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `invoice_vendor_fk_key_idx` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendors` (`VENDOR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `office_letters`
--

DROP TABLE IF EXISTS `office_letters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `office_letters` (
  `OFFICE_LETTER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ATTCHMENT_COUNT` int(2) DEFAULT NULL,
  `KEYWORDS` varchar(200) DEFAULT NULL,
  `TAGS` varchar(300) DEFAULT NULL,
  `INVOICE_ID` int(11) DEFAULT NULL,
  `ORGANIZATION_ID` int(11) DEFAULT NULL,
  `ATTACHMENT_1` blob,
  `ATTACHMENT_2` blob,
  `ATTACHMENT_3` blob,
  `ATTACHMENT_4` blob,
  `ATTACHMENT_5` blob,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`OFFICE_LETTER_ID`),
  KEY `office_letter_invoice_fk_key_idx` (`INVOICE_ID`),
  KEY `office_letter_orgnization_fk_key_idx` (`ORGANIZATION_ID`),
  CONSTRAINT `office_letter_invoice_fk_key_idx` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoices` (`INVOICE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `office_letter_orgnization_fk_key_idx` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organizations` (`ORGANIZATION_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `office_letters`
--

LOCK TABLES `office_letters` WRITE;
/*!40000 ALTER TABLE `office_letters` DISABLE KEYS */;
/*!40000 ALTER TABLE `office_letters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations` (
  `ORGANIZATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(60) DEFAULT NULL,
  `ADDRESS_ID` int(11) DEFAULT NULL,
  `OFFICE_PHONE` varchar(15) DEFAULT NULL,
  `MOBILE` varchar(15) DEFAULT NULL,
  `FAX` varchar(15) DEFAULT NULL,
  `CONTACT_PERSON` varchar(45) DEFAULT NULL,
  `EMAIL` varchar(60) DEFAULT NULL,
  `SEC_EMAIL` varchar(60) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ORGANIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations`
--

LOCK TABLES `organizations` WRITE;
/*!40000 ALTER TABLE `organizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `PROJECT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECT_CODE` varchar(45) DEFAULT NULL,
  `PROJECT_NAME` varchar(60) DEFAULT NULL,
  `PROJECT_DESC` varchar(200) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `VENDOR_ID` int(11) DEFAULT NULL,
  `ADDRESS_ID` int(11) DEFAULT NULL,
  `CLIENT_NAME` varchar(60) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PROJECT_ID`),
  KEY `project_vendor_fk_key_idx` (`VENDOR_ID`),
  KEY `project_address_fk_key_idx` (`ADDRESS_ID`),
  CONSTRAINT `project_address_fk_key_idx` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ADDRESS_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_vendor_fk_key_idx` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendors` (`VENDOR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timesheet_details`
--

DROP TABLE IF EXISTS `timesheet_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timesheet_details` (
  `TIMESHEET_DETAIL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESHEET_ID` int(11) DEFAULT NULL,
  `ATTCHMENT_COUNT` int(2) DEFAULT NULL,
  `ATTACHMENT_1` blob,
  `ATTACHMENT_2` blob,
  `ATTACHMENT_3` blob,
  `ATTACHMENT_4` blob,
  `ATTACHMENT_5` blob,
  `TAGS` varchar(200) DEFAULT NULL,
  `KEYWORDS` varchar(300) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TIMESHEET_DETAIL_ID`),
  KEY `timesheetdetails_timesheet_fk_key_idx` (`TIMESHEET_ID`),
  CONSTRAINT `timesheetdetails_timesheet_fk_key_idx` FOREIGN KEY (`TIMESHEET_ID`) REFERENCES `timesheets` (`TIMESHEET_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timesheet_details`
--

LOCK TABLES `timesheet_details` WRITE;
/*!40000 ALTER TABLE `timesheet_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `timesheet_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timesheets`
--

DROP TABLE IF EXISTS `timesheets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timesheets` (
  `TIMESHEET_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESHEET_TITLE` varchar(60) DEFAULT NULL,
  `WEEK_DATE` date DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `VENDOR_ID` int(11) DEFAULT NULL,
  `PROJECT_ID` int(11) DEFAULT NULL,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TIMESHEET_ID`),
  KEY `timesheet_vendor_fk_key_idx` (`VENDOR_ID`),
  KEY `timesheet_project_fk_key_idx` (`PROJECT_ID`),
  KEY `timesheet_employee_fk_key_idx` (`EMPLOYEE_ID`),
  CONSTRAINT `timesheet_employee_fk_key_idx` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `employees` (`EMPLOYEE_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `timesheet_project_fk_key_idx` FOREIGN KEY (`PROJECT_ID`) REFERENCES `projects` (`PROJECT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `timesheet_vendor_fk_key_idx` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendors` (`VENDOR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timesheets`
--

LOCK TABLES `timesheets` WRITE;
/*!40000 ALTER TABLE `timesheets` DISABLE KEYS */;
/*!40000 ALTER TABLE `timesheets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) DEFAULT NULL,
  `EMAIL` varchar(60) DEFAULT NULL,
  `ROLE` varchar(10) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '0',
  `IS_VERIFIED` int(1) DEFAULT '0',
  `TEMP_EXPIRE_DATE` timestamp NULL DEFAULT NULL,
  `UPDATED_USER` varchar(45) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (262,'kpsvas@gmail.com','9f8a4b2e9dfac9946991d0703e895a011361f318','kpsvas@gmail.com','ADMIN',1,1,NULL,NULL,'2019-12-21 17:07:13');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `VENDOR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `VENDOR_NAME` varchar(60) DEFAULT NULL,
  `VENDOR_CODE` varchar(15) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `OFFICE_PHONE` varchar(15) DEFAULT NULL,
  `OWNER_NAME` varchar(45) DEFAULT NULL,
  `SEC_EMAIL` varchar(60) DEFAULT NULL,
  `FAX` varchar(15) DEFAULT NULL,
  `CONTACT_NAME` varchar(45) DEFAULT NULL,
  `MOBILE` varchar(15) DEFAULT NULL,
  `ADDRESS_ID` int(11) DEFAULT NULL,
  `BANK_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `LAST_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`VENDOR_ID`),
  KEY `customer_user_fk_key_idx` (`USER_ID`),
  KEY `vendor_user_fk_key_idx` (`USER_ID`),
  KEY `vendor_address_fk_key_idx` (`ADDRESS_ID`),
  KEY `vendor_bank_fk_key_idx` (`BANK_ID`),
  CONSTRAINT `vendor_address_fk_key_idx` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ADDRESS_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vendor_bank_fk_key_idx` FOREIGN KEY (`BANK_ID`) REFERENCES `banks` (`BANK_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `vendor_user_fk_key_idx` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors`
--

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-21 12:16:39
