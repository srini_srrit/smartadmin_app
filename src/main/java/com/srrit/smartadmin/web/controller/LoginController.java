package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.app.constants.MessageConstants;
import com.srrit.smartadmin.web.configuration.Messages;
import com.srrit.smartadmin.web.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SessionAttributes({"currentUser", "currentUserId"})
@Controller
public class LoginController {
    private static final Logger log = LogManager.getLogger(LoginController.class);
    @Autowired
    private Messages messages;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(@RequestParam(value = "error", required = false) String error,
                            HttpServletRequest request,HttpServletResponse response, Model model) {

        if (error != null) {
            model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
        }

        response.setStatus(HttpStatus.OK.value());
        //request.getSession().setAttribute("visits", getSiteVisits(request));

        return "user/login";
    }
    //customize the error message
    private String getErrorMessage(HttpServletRequest request, String key){

        Exception exception = (Exception) request.getSession().getAttribute(key);
        String error = "";
        if(exception instanceof UsernameNotFoundException) {
            error = messages.get(MessageConstants.USER_NOT_FOUND);
        }else if (exception instanceof BadCredentialsException) {
            error = messages.get(MessageConstants.INVALID_USER);
        }else if (exception instanceof AccountExpiredException) {
            error = messages.get(MessageConstants.USER_NOT_VERIFIED);
        }else if (exception instanceof DisabledException) {
            error = messages.get(MessageConstants.USER_DISABLED);
        }else{
            error = "Invalid username and/or password";
        }
        return error;
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return "redirect:/login?logout";
    }

    @RequestMapping("/login-success")
    public String loginSuccess(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
        // read principal out of security context and set it to session
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        validatePrinciple(authentication.getPrincipal());
        User loggedInUser = (User) authentication.getPrincipal();

        model.addAttribute("currentUserId", loggedInUser.getUserId());
        model.addAttribute("currentUser", loggedInUser.getUsername());
        session.setAttribute("userId", loggedInUser.getUserId());
        return "redirect:/";

    }
    @RequestMapping("/error-forbidden")
    public String errorForbidden() {
        return "error-forbidden";
    }

    private void validatePrinciple(Object principal) {
        if (!(principal instanceof User)) {
            throw new  IllegalArgumentException("Principal can not be null!");
        }
    }
}
