package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.domain.Address;
import com.srrit.smartadmin.web.domain.Employee;
import com.srrit.smartadmin.web.dto.*;
import com.srrit.smartadmin.web.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class AdminController {

    @GetMapping(value = "/adminForm")
    public String createAdminForm(Model model)
    {
        model.addAttribute("addressDTO",new AddressDTO());
        model.addAttribute("employeeDTO",new EmployeeDTO());
        model.addAttribute("vendorDTO",new VendorDTO());
        model.addAttribute("bankDTO",new BankDTO());
        model.addAttribute("customerDTO",new CustomerDTO());
        return "admin/adminForm";
    }

    @GetMapping("/mailBox")
    public String mailBox()
    {
        return "admin/mailBox";
    }



}
