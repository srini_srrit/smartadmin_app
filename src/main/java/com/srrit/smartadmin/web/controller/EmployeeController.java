package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.domain.Employee;
import com.srrit.smartadmin.web.dto.EmployeeDTO;
import com.srrit.smartadmin.web.handler.EmployeeHandler;
import com.srrit.smartadmin.web.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(value = "/addEmployee")
    public String addEmployee(@Valid EmployeeDTO employeeDTO, HttpServletRequest request, Model model, BindingResult result)
    {
         Employee employee = EmployeeHandler.getEmployee(employeeDTO);
         employeeService.saveEmployee(employee);
         model.addAttribute("mesg","Employee details has been posted");
         return "success";
    }

    @GetMapping(value = "/listEmployees")
    public String employeeList(Model model)
    {
        List<Employee> list = (List<Employee>) employeeService.listAllEmployees(sortByLastUpdatedDesc());
        model.addAttribute("employeeList",list);
        return "employee/employeeList";
    }
    private Sort sortByLastUpdatedDesc() {
        return new Sort(Sort.Direction.DESC, "lastUpdated");
    }

    @GetMapping(value = "/employeeDetails")
    public String empDetails(@RequestParam("id") Integer id,Model model,HttpServletRequest request)
    {
        Employee details = employeeService.getEmployeeById(id);
        model.addAttribute("empDetails",details);
        return "employee/empDetails";
    }

    @PostMapping(value = "/updateEmployee")
    public String updateEmployee(@Valid EmployeeDTO employeeDTO, HttpServletRequest request, Model model,BindingResult result)
    {

        Employee existingemployee = employeeService.getEmployeeByUserName(employeeDTO.getSecEmail());
        if (existingemployee != null) {

        existingemployee.setFirstName(employeeDTO.getFirstName());
        existingemployee.setLastName(employeeDTO.getLastName());
        //employee.setSecEmail(employeeDTO.getSecEmail());
        existingemployee.setEducationDetails(employeeDTO.getEducationDetails());
        existingemployee.setSkills(employeeDTO.getSkills());

        employeeService.saveEmployee(existingemployee);
        model.addAttribute("mesg","Employee profile has been updated");
        return "success";

        }
        else
            model.addAttribute("mesg","Employee Details Not Found, Please Register");
            return "success";

    }


}
