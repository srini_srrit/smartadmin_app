package com.srrit.smartadmin.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.AbstractController;

@Controller
public class StaticContentController  {
	  
	public static final Log logger = LogFactory.getLog(StaticContentController.class);


	@Value("$ {SENDER_USERNAME}")
    private String senderEmail;


	@GetMapping(value = "coming-soon")
	public String comingSoonMapping(){
	        return "comingSoon";
	     }


	@GetMapping("/vendorList")
	public String vendorList()
	{
		return "pages/vendorList";
	}
	@GetMapping("/bankDetails")
	public String bankList()
	{
		return "pages/bankDetails";
	}

	@GetMapping("/empProfile")
	public String empProfile()
	{
		return "employee/empProfile";
	}

	@GetMapping("/vendorProfile")
	public String vendorProfile()
	{
		return "pages/vendorProfile";
	}

	@GetMapping("/showCalendar")
	public String showCalendar()
	{
		return "pages/calendar";
	}

}
