package com.srrit.smartadmin.web.controller;

 import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
 
 	@GetMapping("/")
	public String welcome(Model uiModel) { 
 	 	return "index";
	}

	@GetMapping("home2")
	public String index2() {

		return "index2";
	}

	@GetMapping("home3")
	public String index3() {

		return "index3";
	}

	@GetMapping("/forms")
	public String form(){

 		return "pages/adminForm";
	}

}
