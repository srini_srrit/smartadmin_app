package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.app.constants.EmailConstants;
import com.srrit.smartadmin.web.app.constants.MessageConstants;
import com.srrit.smartadmin.web.bean.ContactUsForm;
import com.srrit.smartadmin.web.bean.EmailBean;
import com.srrit.smartadmin.web.configuration.Messages;
import com.srrit.smartadmin.web.domain.Feedback;
import com.srrit.smartadmin.web.repository.FeedbackRepository;
import com.srrit.smartadmin.web.service.EmailService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class ContactUsController  {
	
	public static final Log logger = LogFactory.getLog(ContactUsController.class); 

	@Autowired
	private FeedbackRepository feedbackRepository; 
	
	@Autowired
	private EmailService emailService;
	  
	
	@Autowired
    private Messages messages;
	
	@Value("$ {app.url}")
    private String siteUrl;  
	

	@Value("$ {SENDER_USERNAME}")
    private String senderEmail; 
	
	 @RequestMapping(value = "contact-us", method = RequestMethod.GET)
	   public String showCommonFeedbackForm(Model uiModel) {

			uiModel.addAttribute("contact", new ContactUsForm());

			return "common/contact";
		}

		@RequestMapping(value = "contact-us", method = RequestMethod.POST)
		public String submitCommonFeedback(@Valid ContactUsForm contact, BindingResult results, HttpServletRequest request, Model uiModel) {

				Feedback feedback = new Feedback();
				feedback.setEmail(contact.getEmail());
				feedback.setSubject(contact.getSubject());
				feedback.setMessage(contact.getMessage()); 
				feedback.setName(contact.getName());
				feedbackRepository.save(feedback); 
				
				EmailBean email = new EmailBean();
				email.setSubject(EmailConstants.CONTACTUS_EMAIL_SUBJECT);
				email.setToEmail(senderEmail);   //TODO we need to update correct email id here
				email.setFeedback(feedback);
				emailService.sendEmail(email, EmailConstants.EMAILBODY_FEEDBACK);
				
				uiModel.addAttribute("mesg", messages.get(MessageConstants.CONTACTUS_MESG));
				return "success";
			 
		}
}
