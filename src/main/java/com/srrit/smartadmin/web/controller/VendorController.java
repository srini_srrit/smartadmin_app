package com.srrit.smartadmin.web.controller;


import com.srrit.smartadmin.web.domain.Vendor;

import com.srrit.smartadmin.web.dto.VendorDTO;
import com.srrit.smartadmin.web.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class VendorController {

    @Autowired
    private VendorService vendorService;

    @PostMapping(value = "/addVendor")
    public String addVendor(@Valid Vendor vendor, Model model, HttpServletRequest request, BindingResult result)
    {

        Vendor vendor1 = vendorService.saveVendor(vendor);
        model.addAttribute("mesg","New Vendor has been added");
        return "success";
    }

    @RequestMapping(value = "/listVendors", method = RequestMethod.GET)
    public String findAllVendors(Model model){

        List<Vendor> list = vendorService.listAllVendors(sortByLastUpdatedDesc());
        model.addAttribute("vendors", list);
        return "vendor/vendorsList";
    }
    private Sort sortByLastUpdatedDesc() {
        return new Sort(Sort.Direction.DESC, "lastUpdated");
    }


    @GetMapping(value = "/vendorDetails")
    public String vendorDetails(@RequestParam("id") Integer id, Model model, HttpServletRequest request)
    {
        Vendor details = vendorService.getVendorById(id);
        model.addAttribute("vendorDetails",details);
        return "vendor/vendorProfile";
    }

    @PostMapping(value = "/updateVendor")
    public String updateVendor(@Valid VendorDTO vendorDTO, HttpServletRequest request, Model model, BindingResult result)
    {

        Vendor excistingVendor = vendorService.getVendorByUserName(vendorDTO.getSecEmail());
        if (excistingVendor != null) {

            excistingVendor.setVendorName(vendorDTO.getVendorName());
            excistingVendor.setCompanyName(vendorDTO.getCompanyName());
            excistingVendor.setMobile(vendorDTO.getMobile());
            excistingVendor.setAbout(vendorDTO.getAbout());
            vendorService.saveVendor(excistingVendor);

            model.addAttribute("mesg","vendor has been updated");
            return "success";

        }
        else

            model.addAttribute("mesg","Please Register");
        return "success";
    }
}
