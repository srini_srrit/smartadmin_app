package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.domain.Customer;
import com.srrit.smartadmin.web.domain.Employee;
import com.srrit.smartadmin.web.dto.CustomerDTO;
import com.srrit.smartadmin.web.handler.CustomerHandler;
import com.srrit.smartadmin.web.service.CustomerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 
 * @author Srini K
 *
 */
@Controller
public class CustomerController  {

	public static final Log logger = LogFactory.getLog(CustomerController.class);

	@Autowired
	private CustomerService customerService;

	@PostMapping(value = "/addCustomer")
	public String saveCustomer(@Valid CustomerDTO customerDTO, HttpServletRequest request, Model model, BindingResult result)
	{
		Customer customer = CustomerHandler.getCustomer(customerDTO);
		customerService.saveCustomer(customer);
		model.addAttribute("mesg","New Customer has been created");
		return "success";
	}

	@RequestMapping(value = "/customersList", method = RequestMethod.GET)
	public String findAllCustomers(Model model){

		List<Customer> list = (List<Customer>) customerService.listAllCustomers(sortByLastUpdatedDesc());
		model.addAttribute("customers", list);
		return "customer/customersList";
	}
	private Sort sortByLastUpdatedDesc() {
		return new Sort(Sort.Direction.DESC, "lastUpdated");
	}


	@GetMapping(value = "/customerDetails")
	public String customerDetails(@RequestParam("id") Integer id, Model model, HttpServletRequest request)
	{
		Customer details = customerService.getCustomerById(id);
		model.addAttribute("customerDetails",details);
		return "customer/customerDetails";
	}

	@PostMapping(value = "/updateCustomer")
	public String updateCustomer(@Valid CustomerDTO customerDTO, HttpServletRequest request,Model model,BindingResult result)
	{

        Customer excistingCustomer = customerService.getCustomerByUserName(customerDTO.getSecEmail());
        if (excistingCustomer != null) {

			excistingCustomer.setFirstName(customerDTO.getFirstName());
			excistingCustomer.setLastName(customerDTO.getLastName());
			excistingCustomer.setMobile(customerDTO.getMobile());
			customerService.saveCustomer(excistingCustomer);

			model.addAttribute("mesg","customer has been updated");
			return "success";

		}
        else

        	model.addAttribute("mesg","Please Register");
        	return "success";
	}



}
