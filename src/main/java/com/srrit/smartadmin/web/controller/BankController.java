package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.domain.Employee;
import com.srrit.smartadmin.web.dto.BankDTO;
import com.srrit.smartadmin.web.dto.EmployeeDTO;
import com.srrit.smartadmin.web.handler.BankHandler;
import com.srrit.smartadmin.web.handler.EmployeeHandler;
import com.srrit.smartadmin.web.repository.BankRepository;
import com.srrit.smartadmin.web.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class BankController {

    @Autowired
    private BankService bankService;

    @PostMapping(value = "/addBank")
    public String createBank(@Valid BankDTO bankDTO, HttpServletRequest request, Model model, BindingResult result){

        Bank bank = BankHandler.getBank(bankDTO);
        bankService.saveBank(bank);
        model.addAttribute("mesg","Bank details has been posted");


        return "success";
    }

}
