package com.srrit.smartadmin.web.controller;

import com.srrit.smartadmin.web.app.constants.AppConstants;
import com.srrit.smartadmin.web.app.constants.EmailConstants;
import com.srrit.smartadmin.web.app.constants.MessageConstants;
import com.srrit.smartadmin.web.bean.EmailBean;
import com.srrit.smartadmin.web.configuration.Messages;
import com.srrit.smartadmin.web.domain.Customer;
import com.srrit.smartadmin.web.domain.Role;
import com.srrit.smartadmin.web.domain.User;
import com.srrit.smartadmin.web.repository.UserRepository;
import com.srrit.smartadmin.web.service.CustomerService;
import com.srrit.smartadmin.web.service.EmailService;
import com.srrit.smartadmin.web.service.EncryptionService;
import com.srrit.smartadmin.web.service.UserService;
import com.srrit.smartadmin.web.util.HashGenerator;
import com.srrit.smartadmin.web.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;

@Controller
public class UserController  {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);  
	
	@Autowired
    private Messages messages;
	
	@Autowired
    private UserRepository userRepository;

	@Autowired
	private EmailService emailService;  

	@Autowired
	private UserService userService;
	
	@Autowired
	private CustomerService customerService;
	
	@Value("$ {account.resetpassword}")
    private String resetPasswordLink; 
	
	@Value("$ {account.verifylink}")
    private String verifylink; 
	
	@Value("$ {app.url}")
    private String siteUrl; 
	
	@Value("$ {account.passwordreseturl}")
    private String passwordResetUrl;  
	
	@Value("$ {SENDER_USERNAME}")
	private String senderEmail;

	@RequestMapping(value = "/signupForm", method = RequestMethod.GET)
	public String addUser(@ModelAttribute("user") User user, Model model) {
		model.addAttribute("url", "signup");
		return "addUser";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String addUser(@Valid @ModelAttribute("user") User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "addUser";
		}

		User dbUser = this.userService.getUserByUsername(user.getUsername());

		if (dbUser != null) return "redirect:/signupForm";

		user.setRole(Role.USER);
		userService.addUser(user);

		EmailBean email = new EmailBean();
		email.setToEmail(user.getEmail());
		email.setSubject(EmailConstants.USER_WELCOME);
		String encryptedUrl = null;
		encryptedUrl = EncryptionService.encodeStandard(user.getEmail());
		email.setAccountVerifyLink(verifylink + encryptedUrl);
		email.setUser(user);
		emailService.sendEmail(email, EmailConstants.EMAILBODY_USER_WELCOME);
		model.addAttribute("mesg", messages.get(MessageConstants.SIGNUP_SUCCESS_MESG) +" <b>"+senderEmail+"</b>");
		return "success";
	}

	@RequestMapping(value="/verifyAccount", method = RequestMethod.GET)
	public String verifyAccount(HttpServletRequest request,
			HttpServletResponse response, Model uiModel) { 
		
		String userName = request.getParameter("verifyUser");
		LOGGER.debug("Verify user name encoded :" + userName);
		userName = EncryptionService.decodeStandard(userName);
		LOGGER.debug("Verify user name :" + userName);
		
		User user = userService.getUserByUsername(userName);

		if (user != null) {
			user.setIsVerified(AppConstants.ACTIVE_STATUS);
			user.setStatus(AppConstants.ACTIVE_STATUS);
			userRepository.save(user);
		}
		uiModel.addAttribute("resetMsg", MessageConstants.ACTIVATED_SUCCESS_MSG);
		return "user/login";
	}   
	
	@RequestMapping(value="/reset-password", method = RequestMethod.GET)
    public String forgotPassword(Model model) { 
		LOGGER.debug("resetting password now...");
        return "user/resetPassword";
	}
 
	@RequestMapping(value="/reset-password", method = RequestMethod.POST)
	public String changeUserPassword(User user, HttpServletRequest req, Model uiModel) {
 		LOGGER.debug("Inside resetpassword method"); 
		user = userService.getUserByUsername(user.getEmail());
		 
		if(user != null) {
		Date expireDate = Utility.getPasswordExpireDate();
		user.setTempExpireDate(expireDate);
		userRepository.save(user);
		
		String encodePass = EncryptionService.encodeStandard(user.getEmail());
		//send an email with reset password link
		EmailBean email = new EmailBean(); 
		email.setSubject(EmailConstants.USER_PASSWORD_RESET);
		email.setToEmail(user.getEmail());  
		email.setUser(user); 
		email.setAccountResetLink(resetPasswordLink + encodePass); 
		email.setPasswordResetLink(passwordResetUrl);
		emailService.sendEmail(email, EmailConstants.EMAILBODY_PASSWORD_RESET);  
		uiModel.addAttribute("templates/email", senderEmail);
	
		}else{
			uiModel.addAttribute("mesg", messages.get(MessageConstants.EMAIL_ALREADY_REGISTERED));  
		}
		
		return "user/resetPassword"; 
	}
	
	@RequestMapping(value="/changePassword", method = RequestMethod.GET)
	public String userPasswordChange(HttpServletRequest req, Model model) {  
		String username = req.getParameter("resetpassword"); 
		String returnPage = "errorPage";  
		User user = userService.getUserByUsername(EncryptionService.decodeStandard(username));
		if(user != null) { 
			if(Utility.isTempPasswordExpired(user.getTempExpireDate())){ 
				returnPage="user/expiredPassword";
			}else{ 
				model.addAttribute("user",user);
				returnPage="user/changePassword";
			} 
		}else{
			model.addAttribute("mesg", messages.get(MessageConstants.CONTACT_ADMIN)); 
		}
		
		return returnPage;

	}
	
	@RequestMapping(value="/updatePassword", method = RequestMethod.GET)
	public String updatePassword(HttpServletRequest request, Model model,  HttpSession session) {
		String username = EncryptionService.decodeStandard(request.getParameter("username"));
		String returnPage = "errorPage";  
		User user = userService.getUserByUsername(username);
		String currentUsername = (String) session.getAttribute("currentUser");

		Customer customerProfile = customerService.findCustomerByUsername(currentUsername);
		if(user != null && user.getUsername().equalsIgnoreCase(customerProfile.getUser().getUsername())) {
			model.addAttribute("user",user);
			returnPage="user/changePassword"; 
		}else{
			model.addAttribute("mesg", messages.get(MessageConstants.CONTACT_ADMIN)); 
		}
		
		return returnPage;

	}

	@RequestMapping(value="/changePassword", method = RequestMethod.POST)
	public String userPasswordChangePost(HttpServletRequest req, Model uiModel) {
 
		String emailId = req.getParameter("templates/email");
		String password = req.getParameter("password");
		User existingUser = userService.getUserByUsername(emailId);
		if(existingUser != null) {
			existingUser.setPassword(HashGenerator.encryptPassword(password.trim()));  
	 		userRepository.save(existingUser); 
			uiModel.addAttribute("resetMsg", MessageConstants.PASSWORD_CHAGNE_MSG);  
		}
		
		//send email for confirmation about profile changes.
    	EmailBean email = new EmailBean(); 
		email.setUser(existingUser);
		email.setToEmail(existingUser.getEmail()); 
		email.setSubject(EmailConstants.PROFILE_UPDATE_EMAIL_SUBJECT); 
		emailService.sendEmail(email, EmailConstants.PROFILE_UPDATE_EMAIL_TEMPLATE); 
		return "user/login";

	}
  	 
 }
