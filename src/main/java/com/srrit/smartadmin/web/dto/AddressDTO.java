package com.srrit.smartadmin.web.dto;

import com.srrit.smartadmin.web.domain.Address;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddressDTO {

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String state;

    private String zip;

    private String country;

}
