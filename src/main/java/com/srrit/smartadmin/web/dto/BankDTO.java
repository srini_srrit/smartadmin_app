package com.srrit.smartadmin.web.dto;

import com.srrit.smartadmin.web.domain.Address;
import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.domain.User;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BankDTO {

    private String bankName;

    private String bankType;

    private int status;

    private String fax;

    private String ContactName;

    private String ContactNumber;

    private String routingNumber;

    private String accountNumber;

    private String mobile;

    private AddressDTO addressDTO;

    private UserDTO userDTO;

}
