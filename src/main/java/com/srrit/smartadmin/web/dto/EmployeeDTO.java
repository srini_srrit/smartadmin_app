package com.srrit.smartadmin.web.dto;

import com.srrit.smartadmin.web.domain.Address;
import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.domain.User;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmployeeDTO {

    private String firstName;

    private String lastName;

    private String sex;

    private int status;

    private String secEmail;

    private String jobTitle;

    private String phone;

    private String about;

    private String educationDetails;

    private String skills;

    private String notes;

    private AddressDTO addressDTO;

    private BankDTO bankDTO;

    private UserDTO userDTO;


}
