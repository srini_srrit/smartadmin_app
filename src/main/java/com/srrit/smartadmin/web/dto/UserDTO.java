package com.srrit.smartadmin.web.dto;

import com.srrit.smartadmin.web.domain.Address;
import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.domain.Role;
import com.srrit.smartadmin.web.domain.User;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDTO {

    private String username;
    private String password;
    private String email;
    private int isVerified;
    private String updatedUser;
    private int status;
    private Date tempExpireDate;
    private Role role;
}
