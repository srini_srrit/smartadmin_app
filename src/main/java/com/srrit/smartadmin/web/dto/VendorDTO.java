package com.srrit.smartadmin.web.dto;


import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VendorDTO {

    private String vendorName;

    private String vendorCode;

    private String officePhone;

    private String ownerName;

    private String companyName;

    private String secEmail;

    private String fax;

    private String contactName;

    private String mobile;

    private int status;

    private String about;

    private String associatedVendor;

    private String website;

    private String notes;

    private AddressDTO addressDTO;

    private BankDTO bankDTO;

    private UserDTO userDTO;

}
