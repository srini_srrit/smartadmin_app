package com.srrit.smartadmin.web.dto;

import com.srrit.smartadmin.web.domain.Address;
import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.domain.User;
import lombok.*;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerDTO {
    private String firstName;

    private String lastName;

    private String mobile;

    private String officePhone;

    private int status;

    private String contactName;

    private String fax;

    private String secEmail;

    private AddressDTO addressDTO;

    private BankDTO bankDTO;

    private UserDTO userDTO;


}
