package com.srrit.smartadmin.web.handler;

import com.srrit.smartadmin.web.app.constants.AppConstants;
import com.srrit.smartadmin.web.domain.*;
import com.srrit.smartadmin.web.dto.VendorDTO;
import com.srrit.smartadmin.web.util.HashGenerator;
import org.springframework.beans.BeanUtils;

public class VendorHandler {

    public static Vendor getVendor(VendorDTO vendorDTO)
    {
        Vendor vendor = new Vendor();
        BeanUtils.copyProperties(vendorDTO,vendor);

        Bank bank = new Bank();
        BeanUtils.copyProperties(vendorDTO.getBankDTO(),bank);

        Address address = new Address();
        BeanUtils.copyProperties(vendorDTO.getAddressDTO(),address);

        User user = new User();
        BeanUtils.copyProperties(vendorDTO.getUserDTO(),user);

        user.setPassword(HashGenerator.encryptPassword(AppConstants.DEFAULT_PASSCODE));
        user.setRole(Role.USER);
        user.setUsername(user.getEmail());

        bank.setStatus(AppConstants.ACTIVE_STATUS);
        vendor.setStatus(AppConstants.ACTIVE_STATUS);

        vendor.setBank(bank);
        vendor.setAddress(address);
        vendor.setUser(user);



        return vendor;
    }

}
