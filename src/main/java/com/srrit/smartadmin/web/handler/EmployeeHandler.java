package com.srrit.smartadmin.web.handler;

import com.srrit.smartadmin.web.app.constants.AppConstants;
import com.srrit.smartadmin.web.domain.*;
import com.srrit.smartadmin.web.dto.EmployeeDTO;
import com.srrit.smartadmin.web.service.EmployeeService;
import com.srrit.smartadmin.web.util.HashGenerator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeHandler {


    public static Employee getEmployee(EmployeeDTO employeeDTO) {

        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO, employee);

        Address address = new Address();
        BeanUtils.copyProperties(employeeDTO.getAddressDTO(), address);

        User user = new User();
        BeanUtils.copyProperties(employeeDTO.getUserDTO(), user);
        user.setPassword(HashGenerator.encryptPassword(AppConstants.DEFAULT_PASSCODE));
        user.setRole(Role.USER);
        user.setUsername(user.getEmail());

        employee.setUser(user);
        employee.setAddress(address);
        user.setStatus(AppConstants.ACTIVE_STATUS);
        employee.setStatus(AppConstants.ACTIVE_STATUS);

        return employee;

    }

}
