package com.srrit.smartadmin.web.handler;

import com.srrit.smartadmin.web.app.constants.AppConstants;
import com.srrit.smartadmin.web.domain.*;
import com.srrit.smartadmin.web.dto.CustomerDTO;
import com.srrit.smartadmin.web.util.HashGenerator;
import org.springframework.beans.BeanUtils;

public class CustomerHandler {

    public static Customer getCustomer(CustomerDTO customerDTO){

        Customer customer = new Customer();
        BeanUtils.copyProperties(customerDTO,customer);

        Bank bank = new Bank();
        BeanUtils.copyProperties(customerDTO.getBankDTO(),bank);

        Address address = new Address();
        BeanUtils.copyProperties(customerDTO.getAddressDTO(),address);

        User user = new User();
        BeanUtils.copyProperties(customerDTO.getUserDTO(),user);

        user.setPassword(HashGenerator.encryptPassword(AppConstants.DEFAULT_PASSCODE));
        user.setRole(Role.USER);
        user.setUsername(user.getEmail());

        bank.setFax(bank.getFax());
        bank.setStatus(AppConstants.ACTIVE_STATUS);
        customer.setStatus(AppConstants.ACTIVE_STATUS);

        customer.setBank(bank);
        customer.setAddress(address);
        customer.setUser(user);

        return customer;

    }

}
