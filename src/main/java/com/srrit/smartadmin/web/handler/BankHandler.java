package com.srrit.smartadmin.web.handler;

import com.srrit.smartadmin.web.app.constants.AppConstants;
import com.srrit.smartadmin.web.domain.Address;
import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.domain.Role;
import com.srrit.smartadmin.web.domain.User;
import com.srrit.smartadmin.web.dto.BankDTO;
import com.srrit.smartadmin.web.util.HashGenerator;
import org.springframework.beans.BeanUtils;

public class BankHandler {

    public static Bank getBank(BankDTO bankDTO){

        Bank bank = new Bank();
        BeanUtils.copyProperties(bankDTO,bank);

        Address address = new Address();
        BeanUtils.copyProperties(bankDTO.getAddressDTO(),address);

        User user = new User();
        BeanUtils.copyProperties(bankDTO.getUserDTO(),user);

        user.setPassword(HashGenerator.encryptPassword(AppConstants.DEFAULT_PASSCODE));
        user.setRole(Role.USER);
        user.setUsername(user.getEmail());
        bank.setUser(user);
        bank.setAddress(address);

        user.setStatus(AppConstants.ACTIVE_STATUS);
        bank.setStatus(AppConstants.ACTIVE_STATUS);

        return bank;
    }
}
