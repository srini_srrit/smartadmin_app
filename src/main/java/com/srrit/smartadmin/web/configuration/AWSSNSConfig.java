package com.srrit.smartadmin.web.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions; 
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;

@Configuration
public class AWSSNSConfig {
	
	@Value("${sa.aws.access_key_id}")
	private String awsId;

	@Value("${sa.aws.secret_access_key}")
	private String awsKey;
	
	@Value("${sa.sns.region}")
	private String region; 
	
	@Bean
	public AmazonSNS snsClient() {
		
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsId, awsKey);
		AmazonSNS snsClient = AmazonSNSClientBuilder.standard()
								.withRegion(Regions.fromName(region))
		                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
		                        .build();
		
		return snsClient;
	}
}
