package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Vendor;
import com.srrit.smartadmin.web.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VendorServiceImpl implements VendorService {

    @Autowired
    private VendorRepository vendorRepository;

    @Override
    public Vendor saveVendor(Vendor vendor) {
        return vendorRepository.save(vendor);
    }

    @Override
    public List<Vendor> listAllVendors(Sort sortByLastUpdatedDesc) {
        return (List<Vendor>) vendorRepository.findAll();
    }

    @Override
    public Vendor getVendorById(Integer id) {
        return vendorRepository.findById(id).orElse(null);
    }

    @Override
    public Vendor getVendorByUserName(String secEmail) {
        return vendorRepository.getVendorByUserName(secEmail);
    }
}
