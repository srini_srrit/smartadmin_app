package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Vendor;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface VendorService {

    Vendor saveVendor(Vendor vendor);

    List<Vendor> listAllVendors(Sort sortByLastUpdatedDesc);

    Vendor getVendorById(Integer id);

    Vendor getVendorByUserName(String secEmail);
}
