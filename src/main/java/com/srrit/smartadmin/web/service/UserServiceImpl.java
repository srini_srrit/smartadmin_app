package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.User;
import com.srrit.smartadmin.web.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserRepository userRepository;

    public User addUser(User user) {
        return userRepository.save(user);
    }

    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int id) {
        User user = userRepository.findById(id).orElse(null);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException("User id not found with id " + id);
        }
        return user;
    }

    public Collection<GrantedAuthority> getAuthorities(User user) {

        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

        if(user != null && user.getRole() != null) {
            authList.add(new SimpleGrantedAuthority(user.getRole().name()));
        }
        return authList;
    }
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.getUserByUsername(username);
    }

    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }
}