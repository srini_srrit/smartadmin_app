package com.srrit.smartadmin.web.service;

import java.util.List;

import com.srrit.smartadmin.web.domain.Subscribe;
 

public interface SubscribeService {

	Subscribe findSubscribeByEmail(String email);
	
	Subscribe getSubscribeByEmail(String email);
	
	List<Subscribe> findSubscribeByEmailIds(String email);
}
