package com.srrit.smartadmin.web.service;
import com.srrit.smartadmin.web.domain.User;

public interface UserService {

	User addUser(User user);

	Iterable<User> getAllUsers();

	User getUserById(int id);

	User getUserByUsername(String username);

}