package com.srrit.smartadmin.web.service;

/**
 * This is generic type interface used to address the CURD operations in service.
 * 
 * This must reuse in the all service to avoid the redudent code.
 * 
 * @author Ramesh
 * @param <E>
 *
 * @param <E> Entity
 * @param <T> Primary Key data type
 */
public interface GenericCurdOperationService<E, T> extends GenericReadOnlyService<E, T> {

	void saveOrUpdateEntity(E entity);

	void delete(T pk);
}
