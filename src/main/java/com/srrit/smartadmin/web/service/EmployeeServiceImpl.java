package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Customer;
import com.srrit.smartadmin.web.domain.Employee;
import com.srrit.smartadmin.web.repository.CustomerRepository;
import com.srrit.smartadmin.web.repository.EmployeeRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Log logger = LogFactory.getLog(CustomerServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Employee> listAllEmployees(Sort sort) {

        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(Integer id) {

        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);

    }

    @Override
    public void deleteEmployee(Integer id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee findEmployeeByUsername(String username) {

        Employee employee = null;

        try {

            TypedQuery<Employee> query = em.createQuery(
                    "select e from Employee e left join fetch e.user u where u.userStatus = 1 "
                            + "and u.username = :username", Employee.class);

            query.setParameter("username", username);
            employee = query.getSingleResult();

        } catch (Exception nre) {
            logger.error("Exception occured : findCustomerByUsername() - Customer Not Found >>>> " + username);
        }
        return employee;
    }

    @Override
    public Employee findEmployeeByEmail(String email) {

        Employee employee = null;

        try {

            TypedQuery<Employee> query = em.createQuery(
                    "select e from Employee e left join fetch e.user u where u.userStatus = 1 "
                            + "and u.email = :email", Employee.class);

            query.setParameter("templates/email", email);
            employee = query.getSingleResult();

        } catch (Exception nre) {
            logger.error("Exception occured : findCustomerByEmail() - Customer Not Found >>>> " + email);
        }
        return employee;
    }

    @Override
    public Employee getEmployeeByUserName(String secEmail) {
        return employeeRepository.getEmployeeDetailsByEmail(secEmail);
    }
}
