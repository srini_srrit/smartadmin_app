package com.srrit.smartadmin.web.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.srrit.smartadmin.web.domain.Customer;
import com.srrit.smartadmin.web.repository.CustomerRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Log logger = LogFactory.getLog(CustomerServiceImpl.class);
 	 
	@Autowired
	private CustomerRepository customerRepository;
	  
	@PersistenceContext
	private EntityManager em; 
	 
	@Override
	public Iterable<Customer> listAllCustomers(Sort sort) {
		return customerRepository.findAll();
	}

	@Override
	public Customer getCustomerById(Integer id) {
		return customerRepository.findById(id).orElse(null);
	}

	@Override
	public Customer saveCustomer(Customer customer) {
		return customerRepository.save(customer);

	}

	@Override
	public void deleteCustomer(Integer id) {
        customerRepository.deleteById(id);
	}

	@Override
	public Customer findCustomerByUsername(String username) {

        Customer customer = null;
		
		try { 
		  
			TypedQuery<Customer> query = em.createQuery(
					"select c from Customer c left join fetch c.user u where u.userStatus = 1 "
					+ "and u.username = :username", Customer.class);

			query.setParameter("username", username);
            customer = query.getSingleResult();

		} catch (Exception nre) {
			logger.error("Exception occured : findCustomerByUsername() - Customer Not Found >>>> " + username);
		}
		return customer;
	}
	
	@Override
	public Customer findCustomerByEmail(String email) {

        Customer customer = null;
		
		try { 
		  
			TypedQuery<Customer> query = em.createQuery(
					"select c from Customer c left join fetch c.user u where u.userStatus = 1 "
					+ "and u.email = :email", Customer.class);

			query.setParameter("templates/email", email);
            customer = query.getSingleResult();

		} catch (Exception nre) {
			logger.error("Exception occured : findCustomerByEmail() - Customer Not Found >>>> " + email);
		}
		return customer;
	}

	@Override
	public Customer getCustomerByUserName(String secEmail) {
		return customerRepository.getCustomerByUserName(secEmail);
	}
}
