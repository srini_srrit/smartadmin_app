package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.bean.EmailBean;

public interface EmailService {

	void sendEmail(EmailBean email, String templateName);

	void sendScheduledEmail(EmailBean email, String templateName);
	
}

