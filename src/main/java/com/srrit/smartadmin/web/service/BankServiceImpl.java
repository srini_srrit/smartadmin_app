package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Bank;
import com.srrit.smartadmin.web.repository.BankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankRepository bankRepository;


    @Override
    public Bank saveBank(Bank bank) {
        return bankRepository.save(bank);
    }
}
