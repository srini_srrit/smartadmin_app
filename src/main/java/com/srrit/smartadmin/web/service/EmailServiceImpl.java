package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.app.constants.EmailConstants;
import com.srrit.smartadmin.web.bean.EmailBean;
import freemarker.template.Configuration;

import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;


import java.util.HashMap;
import java.util.Map;

@Service
public class EmailServiceImpl implements EmailService{

	@Autowired
	private JavaMailSender emailSender;

	@Value("${SENDER_USERNAME}")
	private String senderEmail;

	private String siteUrl;

	@Autowired
	private Configuration freeMarkerConfigurer;

	@Override
	public void sendEmail(EmailBean email, String templateName) {

		MimeMessagePreparator messagePreparator = mimeMessage -> {

			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
			Template template = freeMarkerConfigurer.getTemplate(templateName, "UTF-8");

			if (email.getToEmail() != null) {
				message.setTo(email.getToEmail());
			}

			if (email.getEmailIds() != null) {
				message.setTo(email.getEmailIds());
			}
			if (email.getCcCopy() != null) {
				message.setCc(email.getCcCopy());
			}
			if (email.getBcCopy() != null) {
				message.setBcc(email.getBcCopy());
			}
			message.setSubject(email.getSubject());
			email.setEmailFrom(senderEmail);
			email.setSiteUrl(siteUrl);
			email.setContactUsLink(siteUrl + EmailConstants.CONTACTUS);
			Map<String, Object> context = new HashMap<String, Object>();
			context.put("templates", email);
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, context);
			message.setText(html, true);
		};

		try {
			emailSender.send(messagePreparator);
		} catch (MailException e) {
			throw new MailSendException(e.getMessage());
		}
	}

	public void sendScheduledEmail(EmailBean email, String templateName)  {

	}
}