package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Bank;

public interface BankService {
    Bank saveBank(Bank bank);
}
