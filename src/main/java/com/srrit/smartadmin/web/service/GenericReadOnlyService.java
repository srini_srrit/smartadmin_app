package com.srrit.smartadmin.web.service;

import java.util.List;

public interface GenericReadOnlyService<E, T> {
	
	List<E> fetchAllRecords();

	E getEntityById(T pk);
}
