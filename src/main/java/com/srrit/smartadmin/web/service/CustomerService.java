package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Customer;
import org.springframework.data.domain.Sort;

public interface CustomerService {
  
	Iterable<Customer> listAllCustomers(Sort sort);

	Customer getCustomerById(Integer id);

	Customer saveCustomer(Customer provider);

	void deleteCustomer(Integer id);

	Customer findCustomerByUsername(String username);

	Customer findCustomerByEmail(String email);

    Customer getCustomerByUserName(String secEmail);
}
