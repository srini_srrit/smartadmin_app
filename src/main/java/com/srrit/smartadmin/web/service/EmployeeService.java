package com.srrit.smartadmin.web.service;

import com.srrit.smartadmin.web.domain.Customer;
import com.srrit.smartadmin.web.domain.Employee;
import org.springframework.data.domain.Sort;

public interface EmployeeService {

    Iterable<Employee> listAllEmployees(Sort sort);

    Employee getEmployeeById(Integer id);

    Employee saveEmployee(Employee employee);

    void deleteEmployee(Integer id);

    Employee findEmployeeByUsername(String username);

    Employee findEmployeeByEmail(String email);

    Employee getEmployeeByUserName(String secEmail);
}
