package com.srrit.smartadmin.web.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.srrit.smartadmin.web.domain.Subscribe;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;


@Service
public class SubscribeServiceImpl implements SubscribeService{
	
	public static final Log logger = LogFactory.getLog(SubscribeServiceImpl.class);	
	
	@PersistenceContext
	private EntityManager em;
	
	public Subscribe findSubscribeByEmail(String email) {
	 	Subscribe subscribe = null;
		
	 	try {
	 		TypedQuery<Subscribe> query = em.createQuery(
					"select s from Subscribe s where s.email =:email and s.status = 1", Subscribe.class);
	 		query.setParameter("templates/email",email);
			subscribe = query.getSingleResult();
	
	 	}catch(Exception e){
	 		logger.error("Exception occured: findSubscribeByEmail() >>> email: " +email  +"\n"+e);	
		}
	 	
	 	return subscribe;
		
	}
	
	public Subscribe getSubscribeByEmail(String email) {
	 	Subscribe subscribe = null;
		
	 	try {
	 		TypedQuery<Subscribe> query = em.createQuery(
					"select s from Subscribe s where s.email =:email", Subscribe.class);
	 		query.setParameter("templates/email",email);
			subscribe = query.getSingleResult();
	
	 	}catch(Exception e){
	 		logger.error("Exception occured: findSubscribeByEmail() >>> email: " +email  +"\n"+e);	
		}
	 	
	 	return subscribe;
		
	}
	
	public List<Subscribe> findSubscribeByEmailIds(String email) {
		List<Subscribe> subscribe = null;
		
	 	try {
	 		TypedQuery<Subscribe> query = em.createQuery(
					"select s from Subscribe s where s.email =:email and s.status = 1", Subscribe.class);
	 		query.setParameter("templates/email",email);
			subscribe = query.getResultList();
	
	 	}catch(Exception e){
	 		logger.error("Exception occured: findSubscribeByEmailIds() >>> email: " +email  +"\n"+e);	
		}
	 	
	 	return subscribe;
		
	}
}
