package com.srrit.smartadmin.web.bean;

import javax.validation.constraints.Pattern;

public class ContactUsForm {

	private String name;

	@Pattern (regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
			+"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			+"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
			message="Invalid Email")
	private String email;

	private String subject;

	private String message;

	private int isExistUser;

	private String isExistingUser;
 
	public String getIsExistingUser() {
		return isExistingUser;
	}

	public void setIsExistingUser(String isExistingUser) {
		this.isExistingUser = isExistingUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getIsExistUser() {
		return isExistUser;
	}

	public void setIsExistUser(int isExistUser) {
		this.isExistUser = isExistUser;
	}

	@Override
	public String toString() {
		return "ContactUsForm [name=" + name + ", email=" + email + ", subject="
				+ subject + ", message=" + message + ", isExistUser="
				+ isExistUser + "]";
	}	

}


