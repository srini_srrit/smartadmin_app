
/**
 * 
 */
package com.srrit.smartadmin.web.bean;

import com.srrit.smartadmin.web.domain.Customer;
import com.srrit.smartadmin.web.domain.Feedback;
import com.srrit.smartadmin.web.domain.User;

import java.util.Date;

/**
 * @author Srini Komatipally
 *
 */
public class EmailBean {
	
	private String primaryEmail;
	private String email;
	private String message;
	private String toEmail;
	private String fromEmail;
	private String subject;
	private String template;
	private Customer customer;
	private User user;
	private String accountVerifyLink;
	private String emailFrom;
	private Feedback feedback;
	private String mailHeader;
	private String accountResetLink; 
	  
	private Date expireDate; 
	private String siteUrl; 
	private String[] emailIds;
	private String ccCopy;
	private String bcCopy;  
	
	private String passwordResetLink;
	
	private String contactUsLink; 
	
	private String unsubsribeLink;  
	
	private String activationUrl;
	
	private String bloodGroup;
	
	public String getPrimaryEmail() {
		return primaryEmail;
	}
	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToEmail() {
		return toEmail;
	}
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public String getFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	} 
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getAccountVerifyLink() {
		return accountVerifyLink;
	}
	public void setAccountVerifyLink(String accountVerifyLink) {
		this.accountVerifyLink = accountVerifyLink;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	} 
	
	public String getMailHeader() {
		return mailHeader;
	}
	public void setMailHeader(String mailHeader) {
		this.mailHeader = mailHeader;
	}
	public Feedback getFeedback() {
		return feedback;
	}
	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}
	public String getAccountResetLink() {
		return accountResetLink;
	}
	public void setAccountResetLink(String accountResetLink) {
		this.accountResetLink = accountResetLink;
	}  
	public Date getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	} 
	
	public String getSiteUrl() {
		return siteUrl;
	}
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}
 
	public String[] getEmailIds() {
		return emailIds;
	}
	public void setEmailIds(String[] emailIds) {
		this.emailIds = emailIds;
	}
	public String getCcCopy() {
		return ccCopy;
	}
	public void setCcCopy(String ccCopy) {
		this.ccCopy = ccCopy;
	}
	public String getBcCopy() {
		return bcCopy;
	}
	public void setBcCopy(String bcCopy) {
		this.bcCopy = bcCopy;
	} 
	
	 
	public String getPasswordResetLink() {
		return passwordResetLink;
	}
	public void setPasswordResetLink(String passwordResetLink) {
		this.passwordResetLink = passwordResetLink;
	}
	public String getContactUsLink() {
		return contactUsLink;
	}
	public void setContactUsLink(String contactUsLink) {
		this.contactUsLink = contactUsLink;
	} 
	public String getUnsubsribeLink() {
		return unsubsribeLink;
	}
	public void setUnsubsribeLink(String unsubsribeLink) {
		this.unsubsribeLink = unsubsribeLink;
	} 
 
	public String getActivationUrl() {
		return activationUrl;
	}
	public void setActivationUrl(String activationUrl) {
		this.activationUrl = activationUrl;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}   
}
