package com.srrit.smartadmin.web.bean;

public class ErrorResponse {
    private int code;
    private String developerMessage;

    public ErrorResponse(int code, String developerMessage) {
        this.code = code;
        this.developerMessage = developerMessage;
    }

    public int getCode() {
        return code;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }
}