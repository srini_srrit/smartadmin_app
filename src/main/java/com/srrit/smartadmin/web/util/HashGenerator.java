package com.srrit.smartadmin.web.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HashGenerator { 

	private static final Log logger = LogFactory.getLog(HashGenerator.class);
	
	public static String encryptPassword(String pass) {

		StringBuffer sb = new StringBuffer(); 

		try {
			MessageDigest mDigest = MessageDigest.getInstance("SHA1");
			byte[] result = mDigest.digest(pass.getBytes());

			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
			} 

		}catch(NoSuchAlgorithmException nae) {
			logger.error(String.format("NWHashGenerator : encryptPassword (), NoSuchAlgorithmException occured : %s", nae.getMessage())); 
			
		}catch(Exception e){
			logger.error(String.format("NWHashGenerator : encryptPassword (), Exception occured : %s", e.getMessage())); 
			
		}
		return sb.toString(); 
	}	
	
	public static void main(String[] args) {
		System.out.println(encryptPassword("kpsvas"));
	}
}
