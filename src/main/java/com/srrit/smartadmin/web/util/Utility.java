package com.srrit.smartadmin.web.util;

import java.math.BigDecimal; 
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.srrit.smartadmin.web.app.constants.AppConstants;
import com.srrit.smartadmin.web.domain.Customer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;

public class Utility {  
	
	public static final Log logger = LogFactory.getLog(Utility.class);

	public static List<String> getPhoneNumbers(List<Customer> customersList) {

		List<String> phoneNumbers = new ArrayList<>();
		for(Customer customer : customersList) {
			phoneNumbers.add(customer.getOfficePhone());
		}

		return phoneNumbers;
	}


	public boolean validateEmail(final String hex) {
		Pattern  pattern = Pattern.compile(AppConstants.EMAIL_PATTERN);
		Matcher  matcher = pattern.matcher(hex);
		return matcher.matches(); 
	}
	
	public static int generateRandomInteger(int aStart, int aEnd){

		Random aRandom = new Random();

		if (aStart > aEnd) {
			return 100;
		}
		//get the range, casting to long to avoid overflow problems
		long range = (long)aEnd - (long)aStart + 1;
		// compute a fraction of the range, 0 <= frac < range
		long fraction = (long)(range * aRandom.nextDouble());
		int randomNumber =  (int)(fraction + aStart);

		return randomNumber;

	}
	
	public static long generateRandomLong(long aStart, long aEnd){

		Random aRandom = new Random();

		if (aStart > aEnd) {
			return 100;
		}
		//get the range, casting to long to avoid overflow problems
		long range = (long)aEnd - (long)aStart + 1;
		// compute a fraction of the range, 0 <= frac < range
		long fraction = (long)(range * aRandom.nextDouble());
		long randomNumber =  (long)(fraction + aStart);

		return randomNumber;

	}
	/*public static <E> boolean in(E value, E...set) {		

		return Arrays.asList(set).contains(value);	
	}

	public static <E> boolean notIn(E value, E...set) {		

		return !in(value, set);	
	}
*/

	public static boolean isInteger(String str) {

		try{

			Integer.valueOf(str.trim());			
			return true;

		}catch(Exception e){			
			return false;
		}
	}
	
	public static boolean isIntegerNumber(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}

	public static boolean isDouble(String str) {

		try{

			Double.valueOf(str.trim());			
			return true;

		}catch(Exception e){			
			return false;
		}
	}

	public static boolean isDate(String value, String datePattern, boolean strict) {

		Date date = getAsDate(value, datePattern);

		if(null == date || (strict && datePattern.length() != value.length())){ 
			return false;
		}

		return true;

	}


	public static Date getAsDate(String value, String datePattern) {

		if(null == value || null == datePattern || datePattern.length() <= 0) { 
			return null;
		}

		try {
			DateTimeFormatter pattern = DateTimeFormat.forPattern(datePattern);
			return pattern.parseDateTime(value).toDate();		

		}catch(IllegalArgumentException e) {
			return null;
		}
	}


	public static Double getDoubleFromString(String integerPart, String decimalPart) {		

		if(isInteger(integerPart) || isInteger(decimalPart) || Integer.valueOf(decimalPart) >= 0) {

			return Double.valueOf(integerPart+"."+decimalPart);
		}

		return null;		
	}

	/*public static List<String> convertToList(String s) {		
		return convertToList(s, ",");
	}

	private static List<String> convertToList(String s, String delim) {

		if(s.contains(",")){
			Iterable<String> iterable = Splitter.on(delim).omitEmptyStrings().trimResults().split(s);
			return Lists.newArrayList(iterable);	
		}else{
			return Lists.newArrayList(s);
		}
	}*/

	public static long getDaysDifference(String startDate, String endDate, String pattern) {

		Date sDate = getAsDate(startDate, pattern);
		Date eDate = getAsDate(endDate, pattern);

		return TimeUnit.MILLISECONDS.toDays(eDate.getTime() - sDate.getTime());		
	}

	public static String parseDate (Date date, String pattern) {

		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);		
		return formatter.print(date.getTime());	
	}

	public static String convertDateFormat(String date, String targetPattern, String inputPattern) {

		Date parsedDate = getAsDate(date, inputPattern);		
		return parseDate(parsedDate, targetPattern);	
	}

	public static String converSDtoFixedDecimal(String format, String arg) {

		return String.format(format, Double.parseDouble(arg));
	}

	public static <E> String getTrimmedString(E value) {

		return null == value? "": value.toString();
	}

	public static <E> Double getDoubleValue(E value) {

		if(null == value) {
			return 0.00;
		}

		if(!isDouble(value.toString())){
			return 0.00;
		}

		return Double.valueOf(value.toString());
	}

	/**
	 * This function checks whether the String passed is Null.
	 * 
	 * @param String
	 * @return boolean true if String passed is null.
	 */
	public static boolean isNull(String str) {
		boolean success = false;
		if (str == null) {
			success = true;
		}
		return success;
	}

	/**
	 * This function checks whether the String passed is Empty.
	 * 
	 * @param String
	 * @return boolean true if String passed is null or empty.
	 */

	public static boolean isEmpty(String str) {
		boolean success = false;
		if (str == null || str.trim().length() == 0) {
			success = true;
		}
		return success;
	}

	/**
	 * This function checks whether the Object passed is null.
	 * 
	 * @param Object
	 * @return boolean true if Object passed is null.
	 */
	public static boolean isNullObject(Object obj) {
		boolean success = false;
		if (null == obj) {
			success = true;
		}
		return success;
	}

	/**
	 * This function checks whether the String passed is numerice. It accepts as
	 * both as integer as well as double.
	 * 
	 * @param String
	 * @return boolean true if String passed is numeric.
	 */
	public static boolean isNumeric(String str) {
		boolean success = false;
		try {
			if (!isEmpty(str)) {
				Double.parseDouble(str.trim());
				success = true;
			}
		} catch (NumberFormatException e) {
		}
		return success;
	}

	/**
	 * This function checks whether the String passed is a Valid SSN number. It
	 * accepts the following two formats (NNN-NN-NNNN / NNNNNNNNN)
	 * 
	 * @param String
	 * @return boolean true if String passed is a Valid SSN.
	 */
	public static boolean isValidSSN(String str) {
		boolean success = false;
		int iLength = 0;
		str = str.trim();
		if (!isEmpty(str)) // checking not empty string
		{
			iLength = str.length();

			StringBuffer ssnBuffer = new StringBuffer(str); // converted to
			// string buffer for
			// removing -
			if (iLength == 11) { // checks for format(NNN-NN-NNNN)
				if (ssnBuffer.charAt(3) == '-' && ssnBuffer.charAt(6) == '-') { // checks
					// for
					// '-'
					// at
					// position
					// 3
					// and
					// 6
					ssnBuffer.deleteCharAt(3); // remove - at position 3 for
					// converting to
					// format(NNNNNNNNN)
					ssnBuffer.deleteCharAt(5); // since one char is removed the
					// position of '-' at 6 is
					// shifted to 5 and '-' is
					// removed
					iLength = 9; // after removing - at both the places the
					// length is now 9 chars hence
				}
			}
			str = ssnBuffer.toString();
			// checking all the 9 characters or digits only
			if (iLength == 9) {
				for (int i = 0; i < 9; i++) {
					success = isInteger(new Character(str.charAt(i)).toString());
					if (success == false) {
						break;
					}
				}
			}
		}
		return success;
	}

	/**
	 * This function checks whether the String passed contains any whiteSpace
	 * characters
	 * 
	 * @param String
	 * @return boolean true if String passed contains whitespace characters.
	 */
	public static boolean containsWhitespace(String str) {
		boolean success = false;
		if (!(isEmpty(str))) {
			int iLength = str.length();
			for (int i = 0; i < iLength; i++) {
				if (Character.isWhitespace(str.charAt(i))) {
					success = true;
				}
			}
		}
		return success;
	}

	/**
	 * This function checks whether the String passed is a Valid Email String.
	 * It accepts the following two formats (NNN-NN-NNNN / NNNNNNNNN)
	 * 
	 * @param String
	 * @return boolean true if String passed is a Valid SSN.
	 */
	public static boolean isValidEmail(String str) {
		int iPosAttheRate = 0;
		int iPosDot = 0;
		str = str.trim();
		int iLength = str.length();
		boolean success = false;

		if (!(isEmpty(str))) // Email Address is any empty string
		{
			if (!(containsWhitespace(str))) // check if the string contains
				// whitespacecharacters
			{
				iPosAttheRate = str.indexOf('@');
				iPosDot = str.lastIndexOf('.');
				// check for @ is not at first position
				// check for the last dot position is later than at the @
				// position
				// check for the last dot position is atleast before two
				// characters
				if ((iPosAttheRate > 0) && (iPosAttheRate < iPosDot)
						&& (iPosDot < (iLength - 2))) {
					// check for combination of "@.","..","@@",".@" not existing
					if ((str.indexOf("@.") == -1) && (str.indexOf("..") == -1)
							&& (str.indexOf(".@") == -1)) {
						success = true;
					}
				}

			}

		}
		return success;
	}

	/**
	 * This function checks whether the String passed is a alphanumeric String.
	 * It accepts the following characters 'A-Z','0-9','a-z'
	 * 
	 * @param String
	 * @return boolean true if String passed is an alphanumeric String.
	 */
	public static boolean isAlphaNumeric(String str) {
		String validString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		boolean success = false;
		for (int k = 0; k < str.length(); k++) {
			char ch = str.charAt(k);
			if (validString.indexOf(ch) != -1) {
				success = true;
			} else {
				success = false;
				break;
			}
		}

		return success;
	}

	public static boolean isValidAmount(String strAmt) {
		int indexOfDot = strAmt.indexOf(".");
		int length = strAmt.length();
		if (indexOfDot != -1 && length - (indexOfDot + 1) > 4) {
			return false;
		}
		if (indexOfDot != -1 && indexOfDot > 23) {
			return false;
		}
		if (indexOfDot == -1 && length > 23) {
			return false;
		}

		return true;
	}

	public static boolean compareBigDecimal(BigDecimal bd1, BigDecimal bd2) {

		if(bd1 != null && bd2 != null && bd1.compareTo(bd2) == 0) {
			return true;
		}
		return false;		
	}

	public static boolean compareDouble(Double d1, Double d2) {

		if(d1 != null && d2 != null && d1.compareTo(d2) == 0) {
			return true;
		}
		return false;		
	}


	public static boolean compareLong(Long l1, Long l2) {
		if(l1 != null && l2 != null && l1.compareTo(l2) == 0) {
			return true;
		}
		return false;
	}

	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
		return iterable == null ? Collections.<T>emptyList() : iterable;
	} 
	  
	public static Map<String,String> getUSStateCodes(){
		return usStates;
	}
	public static Map<String,String> getIndiaStateCodes(){
		return indiaStates;
	}
	public static Map<String,String> indiaStates ;
	static{
		indiaStates = new LinkedHashMap<String, String>(); 
		indiaStates.put("Telangana","Telangana");
		indiaStates.put("Andhra Pradesh","Andhra Pradesh");
		indiaStates.put("Karnataka","Karnataka");
	} 
	
	public static Map<String,String> usStates;
	static{
		usStates = new TreeMap<String, String>(); 
		usStates.put("AL","Alabama");
		usStates.put("AK","Alaska");
		usStates.put("AZ","Arizona");
		usStates.put("AR","Arkansas");
		usStates.put("CA","California");
		usStates.put("CO","Colorado");
		usStates.put("CT","Connecticut");
		usStates.put("DE","Delaware");
		usStates.put("DC","District Of Columbia");
		usStates.put("FL","Florida");
		usStates.put("GA","Georgia");
		usStates.put("HI","Hawaii");
		usStates.put("ID","Idaho");
		usStates.put("IL","Illinois");
		usStates.put("IN","Indiana");
		usStates.put("IA","Iowa");
		usStates.put("KS","Kansas");
		usStates.put("KY","Kentucky");
		usStates.put("LA","Louisiana");
		usStates.put("ME","Maine");
		usStates.put("MD","Maryland");
		usStates.put("MA","Massachusetts");
		usStates.put("MI","Michigan");
		usStates.put("MN","Minnesota");
		usStates.put("MS","Mississippi");
		usStates.put("MO","Missouri");
		usStates.put("MT","Montana");
		usStates.put("NE","Nebraska");
		usStates.put("NV","Nevada");
		usStates.put("NH","New Hampshire");
		usStates.put("NJ","New Jersey");
		usStates.put("NM","New Mexico");
		usStates.put("NY","New York");
		usStates.put("NC","North Carolina");
		usStates.put("ND","North Dakota");
		usStates.put("OH","Ohio");
		usStates.put("OK","Oklahoma");
		usStates.put("OR","Oregon");
		usStates.put("PA","Pennsylvania");
		usStates.put("RI","Rhode Island");
		usStates.put("SC","South Carolina");
		usStates.put("SD","South Dakota");
		usStates.put("TN","Tennessee");
		usStates.put("TX","Texas");
		usStates.put("UT","Utah");
		usStates.put("VT","Vermont");
		usStates.put("VA","Virginia");
		usStates.put("WA","Washington");
		usStates.put("WV","West Virginia");
		usStates.put("WI","Wisconsin");
		usStates.put("WY","Wyoming");
	}   
	
	public static String getStateName(String stateCode) {
		return usStates.get(stateCode);
	}
	
	public static boolean validateCouponExpireDate(Date expireDate){
		
		boolean flag = true;
		Calendar current = Calendar.getInstance();
		Calendar actual = Calendar.getInstance();
		actual.setTime(expireDate); 
		
		if (actual.after(current)) { 
	         flag = false;
	    } 
	    
		return flag;
	}

	public static Date getPasswordExpireDate() {
		 
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR, now.get(Calendar.HOUR) + AppConstants.PASSWORD_EXPIRE_VAL);
		return now.getTime();
	}

	public static boolean isTempPasswordExpired(Date tempExpireDate) {
		boolean flag = true;  
		Calendar currentDate = Calendar.getInstance(); 
	 	Calendar expiredDate = Calendar.getInstance();
	 	expiredDate.setTime(tempExpireDate);  
		 if (expiredDate.after(currentDate)) {
	       flag = false;      
	     }   
		 
		return flag;
	} 
 
	public static boolean isEmptyStringArray(String[] ticketType) {
		boolean value = false;
		for(int i =0;i < ticketType.length; i++){
			if(StringUtils.isEmpty(ticketType[i])){
				value = true;
			}
		}
		return value;
	} 
	public static String getContactAdvRedirectPath(String urlPath) {  
		String path = null;
		try{
		if(urlPath != null){
			int lastIndex = urlPath.lastIndexOf("/"); 
			 	path = urlPath.substring(0, lastIndex);   
		}  
		
		}catch(Exception e){
			logger.error("Exception occured while processing getLocationName(): "+ urlPath);
		}
		
		return path;
	}  
	
	public static String getEventDateTime(Date datetime, String timeZone) {
		SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd yyy hh:mm a z");
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		return sdf.format(datetime).toString();
	}

	public static Map<String, String> getBloodGroups() { 
		return bloodGroup;
	} 
	
	public static Map<String,String> bloodGroup ;
	static{
		bloodGroup = new LinkedHashMap<String, String>(); 
		bloodGroup.put("A+","A+");
		bloodGroup.put("A-","A-");
		bloodGroup.put("B+","B+");
		bloodGroup.put("B-","B-");	
		bloodGroup.put("O+","O+");
		bloodGroup.put("O-","O-");
		bloodGroup.put("AB+","AB+");
		bloodGroup.put("AB-","AB-");
	} 
	 
}
