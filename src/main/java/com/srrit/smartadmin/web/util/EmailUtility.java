package com.srrit.smartadmin.web.util;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ByteArrayResource; 
import org.springframework.core.io.InputStreamSource;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper; 

public class EmailUtility { 
	
    private static final Log logger = LogFactory.getLog(EmailUtility.class);
    
	public static InputStreamSource convertHtmlPDF(String text) {

		InputStreamSource targetStream = null;
		try {

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();   
			Document document = new Document();
			PdfWriter instance = PdfWriter.getInstance(document, byteArray);
			document.open(); 
			XMLWorkerHelper.getInstance().parseXHtml(instance, document, new StringReader(text));
			document.close(); 

			targetStream =  new ByteArrayResource(byteArray.toByteArray()); 	

		} catch (Exception e) {
			logger.error("Exception occured while converting html to PDF"  + e.getMessage());
			e.printStackTrace();
		}

		return targetStream;
	}

}
