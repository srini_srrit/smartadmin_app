package com.srrit.smartadmin.web.repository;
 
import com.srrit.smartadmin.web.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	 
    User findByEmail(String email);
    User getUserByUsername(String username);
}
