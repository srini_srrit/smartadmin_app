package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Project;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project,Integer> {
}
