package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.TimeSheetDetail;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeSheetDetailsRepository extends PagingAndSortingRepository<TimeSheetDetail,Integer> {
}
