package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Integer> {
    @Query("SELECT e FROM Employee e WHERE secEmail = :secEmail")
    Employee getEmployeeDetailsByEmail(String secEmail);
}
