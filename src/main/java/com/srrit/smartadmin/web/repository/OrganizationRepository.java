package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Organization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends CrudRepository<Organization,Integer> {
}
