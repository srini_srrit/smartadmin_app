package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.InvoiceDetail;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceDetailsRepository extends PagingAndSortingRepository<InvoiceDetail,Integer> {
}
