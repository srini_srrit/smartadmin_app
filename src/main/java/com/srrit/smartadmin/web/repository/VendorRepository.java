package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Vendor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendorRepository extends PagingAndSortingRepository<Vendor, Integer > {

    @Query("SELECT v FROM Vendor v WHERE secEmail = :secEmail")
    Vendor getVendorByUserName(String secEmail);
}
