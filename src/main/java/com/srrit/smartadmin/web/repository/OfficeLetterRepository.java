package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.OfficeLetter;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeLetterRepository extends PagingAndSortingRepository<OfficeLetter,Integer> {
}
