package com.srrit.smartadmin.web.repository;

import org.springframework.data.repository.CrudRepository;

import com.srrit.smartadmin.web.domain.Subscribe;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscribeRepository extends CrudRepository<Subscribe, Integer>{
	   
}
