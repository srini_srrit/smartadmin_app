package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Bank;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankRepository extends PagingAndSortingRepository<Bank,Integer> {
}
