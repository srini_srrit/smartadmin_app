package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Invoice;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends PagingAndSortingRepository<Invoice,Integer> {
}
