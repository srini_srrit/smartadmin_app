package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.TimeSheet;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeSheetRepository extends PagingAndSortingRepository<TimeSheet,Integer> {
}
