package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Feedback;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackRepository extends CrudRepository<Feedback, Integer>{
	   
}
