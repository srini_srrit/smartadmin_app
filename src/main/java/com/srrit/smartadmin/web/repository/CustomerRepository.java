package com.srrit.smartadmin.web.repository;

import com.srrit.smartadmin.web.domain.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer>{

    @Query("SELECT c FROM Customer c WHERE secEmail = :secEmail")
    Customer getCustomerByUserName(String secEmail);



}
