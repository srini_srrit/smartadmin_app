
/**
 * 
 */
package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Srini Komatipally
 *
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Feedback implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FEEDBACK_ID")
	private Integer feedbackId;
	 
	private String name;
	private String email; 
	private String subject;
	
	@Column(name = "EXISTUSER")
	private String existUser;
	private String message;
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;

}
