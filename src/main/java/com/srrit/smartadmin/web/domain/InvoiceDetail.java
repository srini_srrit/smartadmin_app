package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "invoice_details")
public class InvoiceDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer invoicesDetailId;

    @Column(name = "ATTCHMENT_COUNT")
    private int attachmentCount;

    private String keywords;

    private String tags;

    @Column(name = "ATTACHMENT_1")
    private Blob attachment1;

    @Column(name = "ATTACHMENT_2")
    private Blob attachment2;

    @Column(name = "ATTACHMENT_3")
    private Blob attachment3;

    @Column(name = "ATTACHMENT_4")
    private Blob attachment4;

    @Column(name = "ATTACHMENT_5")
    private Blob attachment5;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "INVOICE_ID")
    private Invoice invoice;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;

}
