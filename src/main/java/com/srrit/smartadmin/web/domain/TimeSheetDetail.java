package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="timesheet_details")
public class TimeSheetDetail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
     private int timeSheetDetailId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="TIMESHEET_ID")
     private TimeSheet timeSheet;

    private int ATTCHMENT_COUNT;

    @Column(name="ATTACHMENT_1")
    private Blob attachment1;

    @Column(name="ATTACHMENT_2")
    private Blob attachment2;

    @Column(name="ATTACHMENT_3")
    private Blob attachment3;

    @Column(name="ATTACHMENT_4")
    private Blob attachment4;

    @Column(name="ATTACHMENT_5")
    private Blob attachment5;

    private String tags;

    private String keywords;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

}
