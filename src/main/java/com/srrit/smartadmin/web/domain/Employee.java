package com.srrit.smartadmin.web.domain;

import com.srrit.smartadmin.web.domain.User;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "employees")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer employeeId;

    private String firstName;

    private String lastName;

    private String sex;

    private int status;

    private String secEmail;

    private String jobTitle;

    private String phone;

    private String about;

    private String skills;

    private String notes;

    private String educationDetails;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "BANK_ID")
    private Bank bank;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "USER_ID")
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;



}

