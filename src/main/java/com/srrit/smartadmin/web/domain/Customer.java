package com.srrit.smartadmin.web.domain;

import com.srrit.smartadmin.web.domain.User;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table (name="customers")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customer_id")
	private Integer customerId;

	private String firstName;

	private String lastName;

	private String mobile;

	private String officePhone;

	private int status;

	private String contactName;

  	private String fax;

	private String secEmail;

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "USER_ID")
	private User user;

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "ADDRESS_ID")
	private Address address;

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "BANK_ID")
	private Bank bank;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;

}
