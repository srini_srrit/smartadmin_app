package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "invoices")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer invoiceId;

    private String code;

    private String title;

    private String invoiceType;

    private Date invoiceDate;

    private Date dueDate;

    private Date paidDate;

    private double amount;

    private String desc;

    private String addDetails;

    private int status;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "VENDOR_ID")
    private Vendor vendor;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "PROJECT_ID")
    private Project project;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "TIMESHEET_ID")
    private TimeSheet timeSheet;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;

}
