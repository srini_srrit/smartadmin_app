package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "projects")
public class Project implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int projectId;

  private String projectCode;

  private String projectName;

  private String projectDesc;

  private int status;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name="VENDOR_ID")
  private Vendor vendor;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name="ADDRESS_ID")
  private Address address;

  private String clientName;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="LAST_UPDATED")
  private Date lastUpdated;

}
