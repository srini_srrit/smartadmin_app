package com.srrit.smartadmin.web.domain;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "address")
public class Address implements Serializable { 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer addressId;

	private String address1;

	private String address2;

	private String address3; 

	private String city;

	private String state;

	private String zip;

	private String country;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;


}
