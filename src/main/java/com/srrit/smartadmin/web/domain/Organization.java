package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "organizations")
public class Organization implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int organizationId;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="ADDRESS_ID")
    private Address address;

    private String officePhone;

    private String mobile;

    private String fax;

    private String contactPerson;

    private String email;

    private String secEmail;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATED")
    private Date lastUpdated;

}
