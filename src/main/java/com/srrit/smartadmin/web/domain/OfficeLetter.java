package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="office_letters")
public class OfficeLetter implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int officeletterId;

    @Column(name="ATTCHMENT_COUNT")
    private int attachmentCount;

    private String keywords;

    private String tags;

    @ManyToOne
    @JoinColumn(name="INVOICE_ID")
    private Invoice invoice;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="ORGANIZATION_ID")
    private Organization organization;

    @Column(name = "ATTACHMENT_1")
    private Blob attachment1;

    @Column(name = "ATTACHMENT_2")
    private Blob attachment2;

    @Column(name = "ATTACHMENT_3")
    private Blob attachment3;

    @Column(name = "ATTACHMENT_4")
    private Blob attachment4;

    @Column(name = "ATTACHMENT_5")
    private Blob attachment5;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATED")
    private Date lastUpdated;

}
