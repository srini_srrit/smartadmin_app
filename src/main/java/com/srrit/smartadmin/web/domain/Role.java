package com.srrit.smartadmin.web.domain;

public enum Role {
	USER,
	ADMIN
}
