package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table (name = "time_sheets")
public class TimeSheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int timeSheetId;

    private String timeSheetTitle;

    private Date weekDate;

    private Date startDate;

    private Date endDate;

    @ManyToOne
   @JoinColumn(name="VENDOR_ID")
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name="PROJECT_ID")
    private Project project;

    @ManyToOne
    @JoinColumn(name="EMPLOYEE_ID")
    private Employee employee;

    private int status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATED")
    private Date lastUpdated;


}
