package com.srrit.smartadmin.web.domain;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
 

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Subscribe implements Serializable {    

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;
	 
	private String email;   
	
	private int status;
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;

}