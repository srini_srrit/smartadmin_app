package com.srrit.smartadmin.web.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "vendors")
public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "VENDOR_ID")
    private Integer vendorId;

    private String vendorName;

    private String vendorCode;

    private String officePhone;

    private String ownerName;

    private String companyName;

    private String secEmail;

    private String fax;

    private String contactName;

    private String mobile;

    private int status;

    private String about;

    private String associatedVendor;

    private String website;

    private String notes;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "BANK_ID")
    private Bank bank;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;
}
