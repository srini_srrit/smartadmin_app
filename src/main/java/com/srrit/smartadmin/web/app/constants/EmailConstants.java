package com.srrit.smartadmin.web.app.constants;

public interface EmailConstants {

	public static final String USER_WELCOME = "Thank you message for your Registration";

	public static final String USER_WELCOME_SUBJECT = "Thank you for visiting our site";  
 
	public static final String VERIFY_ACCOUNT_SUBJECT = "TechAdminPro.com: Verify your account";
	public static final String USER_PASSWORD_RESET = "Requested Password Reset for Your TechAdminPro.com account";

	public static final String FEEDBACK_SUBJECT = "Customer Feedback";   
	
	public static final String CONTACTUS_EMAIL_SUBJECT = "TechAdminPro.com: Contact Us info";
	public static final String PROFILE_UPDATE_EMAIL_SUBJECT = "Your TechAdminPro.com account has been updated";
	public static final String UNSUBSCRIBE_EMAIL_SUBJECT = "Re: your unsubscribe request"; 
	
	public static final String SUBSCRIBE_EMAIL_SUBJECT = "Welcome to TechAdminPro.com !";
	
	public static final String EMAILBODY_USER_WELCOME = "templates/welcome.ftl";
	public static final String EMAILBODY_VERIFY_ACCOUNT = "templates/verifyAccount.ftl";
	public static final String EMAILBODY_PASSWORD_RESET = "templates/resetPassword.ftl";

	public static final String EMAILBODY_FEEDBACK = "templates/feedback.ftl";
	
	public static final String EMAILBODY_SUBSCRIBE = "templates/subscribe.ftl";

	public static final String EMAILBODY_UNSUBSCRIBE = "templates/unsubscribe.ftl";

 	public static final String PROFILE_UPDATE_EMAIL_TEMPLATE = "templates/profile.ftl";
 	
	public static final String DONOTREPLY = "***PLEASE NOTE THIS IS A NO REPLY MAIL***";
	
	public static final String emailTo ="srrittest@gmail.com";
	
	public static final String emailFrom ="srrittest@gmail.com";
	
	public static final String CONTACTUS="contact-us";
	 
}
