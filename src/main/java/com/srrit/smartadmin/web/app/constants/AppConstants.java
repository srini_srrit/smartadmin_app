package com.srrit.smartadmin.web.app.constants;

public class AppConstants {

	public static final String ACTIVE_USER = "A";
	public static final String INACTIVE_USER = "I"; 
	
	public static final int ACTIVE_STATUS = 1; //Active
	public static final int INACTIVE_STATUS = 0; //Inactive
	public static final int DISABLE_STATUS = 99; //Disable 
 	 
	
	public static final String ADMIN_USER_ID = "srrit.spandana@gmail.com";
	public static final String DEFAULT_COUNTRY = "IND";

	public static final String GOOGLE_SUCCESS_URL_MAPPING = "google/success"; 
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";	 

	public static final int PASSWORD_EXPIRE_VAL = 24;

	public static final String DEFAULT_PASSCODE = "srrit@01";
    public static final int REQUEST_SENT = 9;
	public static final int REQUEST_ACKNOWLEDGED = 3;
	public static final int REQUEST_FULFILLED = 1;
	public static final int REQUEST_ARCHIVED = 0;

}
