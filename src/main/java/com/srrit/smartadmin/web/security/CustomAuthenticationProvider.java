
package com.srrit.smartadmin.web.security;

import com.srrit.smartadmin.web.util.HashGenerator;
import com.srrit.smartadmin.web.domain.User;
import com.srrit.smartadmin.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException{
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        User user = userService.getUserByUsername(username.toLowerCase());

        if (user == null) {
            throw new UsernameNotFoundException("Username not found.");
        }else if (!(HashGenerator.encryptPassword(password.trim()).equalsIgnoreCase(user.getPassword()))) {
            throw new BadCredentialsException("Wrong password.");
        }else if(user.getIsVerified() == 0) {
            throw new AccountExpiredException("User is not verified.");
        }else if(user.getStatus() == 0) {
            throw new DisabledException("User is disabled.");
        }

        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();

        return new UsernamePasswordAuthenticationToken(user, password, authorities);
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }
}

